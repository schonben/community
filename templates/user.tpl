<section class="page box">
    <img class="user" src="http://avatar.ulfls.com/{$thisone->id}.png?size=120" alt=""/>
	<h2>{$thisone->displayName()}</h2>
	<h3>{$thisone->fname} {$thisone->lname}</h3>

	<hr/>
	<h2>{"Statistics"|loc}</h2>
	{$stats.posts} posts in {$stats.threads} threads.

	<h1>{"Groups"|loc}</h1>
	<ul>
	{foreach $groups as $group}
		<li><a href='group_{$group.id}'>{$group.title}</a></li>
	{/foreach}
	</ul>
    <br class="clear"/>
</section>