<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$query = "
	select
	 	taglist.val,
	 	taglist.nr
	from
		taglist
	order by	taglist.nr desc, taglist.val
	";
$tg = pg_query($conn,$query);

$tags = Array();
while ($tg_r = pg_fetch_array($tg)) {
	$tags[] = Array('val'=>$tg_r['val'],'count'=>$tg_r['nr']);
}
$smarty->assign('tags',$tags);

?>