<section class="page box">
	<h2>{"Tag Management"|loc}</h2>
	<table class='list'>
		<tr>
			<th>{"Tag"|loc}</th>
			<th>{"Uses"|loc}</th>
		</tr>
		{foreach $tags as $tag}
			<tr>
				<td>
					{$tag.val|ucwords|local_format}
				</td>
				<td style="text-align:center;">
					{$tag.count}
				</td>
				<td>
					<a href="?tag={$tag.val|urlencode}">{"Edit Tag"|loc}</a>
				</td>
				<td>
					<a href="communication?tag={$tag.val|urlencode}">{"View Threads"|loc}</a>
				</td>
			</tr>
		{/foreach}
	</table>

</section>
