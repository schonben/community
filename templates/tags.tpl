<form action='tagssave.do' method='post'>
	<section class='page box'>
		<h2>{"Tags"|loc}</h2>
		<input type='submit' value='{"Save"|loc}'/>
		<table class='list'>
			<tr>
				<th>{"Priority"|loc}</th>
				<th>{"Tag"|loc}</th>
				<th>{"Posts"|loc}</th>
                <th>{"Last Post"|loc}</th>
                <th>{"Thread"|loc}</th>
                <th>{"Poster"|loc}</th>
			</tr>
			{foreach $tags as $tag}
				<tr>
					<td class='pri_sel'>
						<input id='tag_{$tag.val}_lo' class='hiddenradio' type='radio' name='tag[{$tag.val}]' {if $tag.priority == -1} checked='checked' {/if} value='-1'/>
						<label for='tag_{$tag.val}_lo' class='pri pri_lo'>-</label>
						<input id='tag_{$tag.val}_me' class='hiddenradio' type='radio' name='tag[{$tag.val}]' {if !$tag.priority} checked='checked' {/if} value='0'/>
						<label for='tag_{$tag.val}_me' class='pri pri_me'> </label>
						<input id='tag_{$tag.val}_hi' class='hiddenradio' type='radio' name='tag[{$tag.val}]' {if $tag.priority == 1} checked='checked' {/if} value='1'/>
						<label for='tag_{$tag.val}_hi' class='pri pri_hi'>+</label>
					</td>
					<td><a href='/communication/{$tag.val|urlencode}'>{$tag.val|ucwords|local_format}</a></td>
					<td style='text-align:center;'>{$tag.count}</td>
                    <td>{$tag.dati->format("Y-m-d H:i")}</td>
                    <td><a href='/thread/{$tag.thread}'>{$tag.subject|local_format}</a></td>
					<td>{$tag.user->displayName()}</td>
				</tr>
			{/foreach}
		</table>
	</section>
</form>
