<script src="/js/post.js" type="text/javascript"></script>

<section class="page box">
	<form id="postform" action="/post.do" method="post">
		{if $action eq "post"}
            <h2>{$title|local_format}</h2>
            {include "threadinfo.tpl"}
        {else}
            <h2>{$post->subject|local_format}</h2>
            <div id="mess" class="reply">
                {$post->display()}
			</div>
			<div style="display:none;" id="bodytext">{$post->body|htmlspecialchars}</div>
			<hr/>
            {if $action eq "reply"}
				<input type="button" value="{"Quote"|loc} &gt;&gt;" onclick="quote(this.form)"/>
			{/if}
		{/if}
		<div class="post_wrap" valign="top">
			<!--<script>edToolbar("post"); </script>-->
			<textarea class="textbox" name="post" id="post" cols="60" rows="15">{$edit|htmlspecialchars}</textarea>
			<br/>
			<input type="submit" value="{"Post"|loc}"/>
		</div>
        <input type="hidden" name="id" value="{$post->id}"/>
        <input type="hidden" name="action" value="{$action}"/>
        <input type="hidden" name="parent" value="{$post->parent}"/>
        <input type="hidden" name="thread" value="{$post->thread}"/>
        <br class="clear"/>
	</form>
</section>
