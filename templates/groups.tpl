<section class="columns">
	<div class="col left">
		<div class="box">
			<h2>{'Public Groups'|loc}</h2>
			<table class="list">
				<tr>
					<th>{'Name'|loc}</th>
					<th>{'Members'|loc}</th>
					<th>{'Threads'|loc}</th>
				</tr>
				{foreach $gr1 as $gr}
					<tr>
						<td><a href="group_{$gr.id}">{$gr.title}</a></td>
						<td>{$gr.members}</td>
						<td>{$gr.threads}</td>
					</tr>
				{/foreach}
			</table>
		</div>

	</div>
	<div class="col right">
		<div class="box">
			<h2>{'My Groups'|loc}</h2>
			<table class="list">
				<tr>
					<th>{'Name'|loc}</th>
					<th>{'Members'|loc}</th>
					<th>{'Threads'|loc}</th>
				</tr>
				{foreach $gr2 as $gr}
					<tr>
						<td><a href="group_{$gr.id}">{$gr.title}</a></td>
						<td>{$gr.members}</td>
						<td>{$gr.threads}</td>
					</tr>
				{/foreach}
			</table>
		</div>
	</div>
</section>