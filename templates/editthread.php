<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$pg_title = loc("Edit");

$extra_css[] = "postBase";
$extra_css[] = "post";

$thread = new thread($id);
$post = new post($thread->getPost());

$smarty->assign('thread',$thread);
$smarty->assign('post',$post);


    // TODO move these to a static method or something. (re-use the code (no duplicates, seriously))
    $sql = "
        SELECT
            val,
            nr
        FROM
            #schema#.taglist
        WHERE
            val != 'general'
        ORDER BY
            val
            ";
    $result = $db->query($sql);
    $smarty->assign('tags',$result->fetchAll());

    $sql = "
        SELECT
            uuid,
            'gr' as tp,
            title as short,
            description as long
        FROM
            #schema#.groups
    
        UNION
    
        SELECT
            uuid,
            'us' as tp,
            nick as short,
            fname || ' ' || lname as long
        FROM
            #schema#.users_info
        ORDER BY
            short
        ";
    $result = $db->query($sql);
    $data = array();
    while ($row = $result->fetch()) {
        $data[$row['uuid']] = array(
            "tp" => $row['tp'],
            "short" => $row['short'],
            "long" => $row['long'],
            );
    }
    $smarty->assign('tolist',$data);
    $smarty->assign('tolist_json',json_encode($data));


?>