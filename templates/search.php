<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$maxthreads = 10;

$sql = "
    SELECT
        *
    FROM
    (
        SELECT
            posts.*,
            threads.*,
            posts.uuid as post_id,
            ts_rank_cd(to_tsvector(posts.body || ' ' || threads.subject),to_tsquery('english',:search)) as rank
        FROM
            community.posts
        LEFT JOIN
            community.threads
        ON
            posts.thread_id = threads.uuid
        ) r
    WHERE
        rank > 0
    AND
        private = false
    ORDER BY
        rank desc,
        thread_id,
        dati desc
    LIMIT
        150
	";
$search = $GLOBALS['db']->prepare($sql);

// Mangle the search string
$query = trim(preg_replace("/[^a-z0-9&_ -]+/i","",$_GET['query']));
$query = trim(preg_replace("/ &? ?/i"," & ",$query));

$search->execute(array(":search"=>$query));
	
$threads = Array();
$threadcount=0;
while (($row = $search->fetch()) && $threadcount < ($maxthreads+1)) {
    if (empty($threads[$row['thread_id']])) $threadcount++;
	if ($threadcount <= $maxthreads) $threads[$row['thread_id']][] = new post($row['post_id']);
}
$smarty->assign("query",$query);
$smarty->assign("threads",$threads);

?>
