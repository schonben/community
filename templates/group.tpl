<section class='page box'>
	<h2>{$title}</h2>
	<p>{$description|local_format}</p>
		
	<ul>
		{foreach $userlist as $row}
			<li>{$row.nick}</li>
		{/foreach}
	</ul>
	<hr/>
	{$threadlist}
</section>
