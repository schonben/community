<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$pg_title = loc("Groups");

$query = "
	select	id,
				title,
				tp,
				members,
				threads
	from		gr_list
	where		tp < 2
	order by	title
	";
$gr1 = pg_query($conn,$query);

$query = "
	select	id,
				title,
				tp,
				members,
				threads
	from		gu_link t1 left join
				gr_list t2
	on			t1.gid = t2.id
	where		t1.uid = $1
	order by	title
	";
$params = Array($_SESSION['id']);
$gr2 = pg_query_params($conn,$query,$params);

$smarty->assign('gr1',pg_fetch_all($gr1));
$smarty->assign('gr2',pg_fetch_all($gr2));

?>