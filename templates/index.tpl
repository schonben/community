<script type='text/javascript'>
	{$script}
</script>

<div class="newdashlets hidden">
    <h3>{"Add Dashlet"|loc}</h3>
    <ul>
        {foreach $newdashlets as $nd}
            <li><a href="javascript: void(0);" onclick="addDashlet('{$nd}')">{$nd}</a></li>
        {/foreach}
    </ul>
</div>

<section class='columns'>
    {foreach $sides as $side}
        <ul id="col_{$side}" class='col {$side} dashlet dashboard'>
        {foreach $row.$side as $dash}
            {$dash}
        {/foreach}
        </ul>
    {/foreach}
</section>