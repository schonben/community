<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$sql = "
	SELECT
	 	taglist.val,
	 	taglist.nr,
	 	taglist.dati,
	 	taglist.user_id,
	 	taglist.thread_id,
	 	taglist.subject,
	 	marker_tag.priority
	FROM
		#schema#.taglist_threads taglist
	LEFT JOIN
		#schema#.marker_tag
	ON
		taglist.val = marker_tag.tag_val
	AND
	   marker_tag.user_id = :user
	ORDER BY
	   taglist.nr desc,
	   taglist.val
	";
$result = $db->prepare($sql);
$result->execute(array(":user"=>$user->id));

$tags = Array();
while ($row = $result->fetch()) {
	$tags[] = Array(
	   'val'=>$row['val'],
	   'count'=>$row['nr'],
       'dati'=>new DateTime($row['dati']),
       'user'=>new user($row['user_id']),
       'thread'=>$row['thread_id'],
       'subject'=>$row['subject'],
	   'priority'=>$row['priority']
       );
}
$smarty->assign('tags',$tags);

?>