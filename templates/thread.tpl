<section class="page box">
    <div class="threadicon">
	    {if $thread->announce}
           <img src="/layout/satlink4/images/icons/announce.png" alt="" title="{"Announcement"|loc}"/>
	    {else}
           <img src="/layout/satlink4/images/icons/message.png" alt="" title="{"Message"|loc}"/>
        {/if}
	</div>
	<div class="userbox">
		<span class="threadtype">
		    {if $thread->private}
                <img src="/layout/satlink4/images/icons/type_private_sm.png" alt="" title="Private"/>
            {else}
                <img src="/layout/satlink4/images/icons/type_public_sm.png" alt="" title="Public"/>
            {/if}
		</span>
		{foreach $thread->getLinks() as $link}
			<div class="threaduser">
                <img src="//avatar.ulfls.com/{$link.uuid}.{$link.tp}.png?size=20&amp;crop=1" alt="{$link.nick}" title="{$link.nick}"/>
			</div>
		{/foreach}
	</div>
	<h2 class="title">{$thread->subject|local_format}</h2>
	<div class="threadtaglist">
		{foreach $thread->getTags() as $tag}
			<span class="threadtagitem">
				{$tag|ucwords}
			</span>
		{/foreach}
	</div>
    {$count=0}
	{foreach $thread->getThread() as $post}
        {if $post->unread}
            <a name="unread{$count++}"/>
        {/if}{$post->display()}
	{/foreach}
</section>
<script type="text/javascript">
	setTimeout(function() {
		$.get("/markread.do", { item: "{$thread->getId()}" } );
	},3000);
</script>
