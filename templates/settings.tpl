<section class='page box'>
	<h2>{"Settings"|loc}</h2>
	
	<form action='settings_save.run' method='post' enctype='multipart/form-data'>
		<label>{"Name"|loc}:</label>
		<span>{$user->fname} {$user->lname}</span><br />
		<label>{"E-mail"|loc}:</label>
		<span>{$user->email}</span><br />
		<label>{"Phone"|loc}:</label>
		<span>{$user->phone}</span><br />
		<label>{"Address"|loc}:</label>
		<span>{$user->street}</span><br />
		<label>&#160; </label><span>{$usr->zip} {$usr->city}</span><br />
		<label>&#160; </label><span>{$usr->country}</span><br />
		<div>
			{"uudb_text"|loc}
			<hr />
		</div>			
		<label for='nick'>{"Nick"|loc}</label>
		<input type='text' id='nick' name='nick' size='20' value='{$user->nick}'/><br />
		<label for='set_locale'>{"Language"|loc}:</label>
		{html_options name='set_locale' options=$locs selected=$usr.locale id='set_locale'}
		<br />
		<hr />
	<div>
		<input type='checkbox' name='msg_mail' value='true'/> {"msg0"|loc}<br />
		<input type='checkbox' name='pri_mail' value='true'/> {"msg1"|loc}
	</div>	
	<hr />
		<label>{"Signature"|loc}:</label>
		<textarea name='sig' cols='40' rows='8' class='text'>{$usr.sig}</textarea><br />
	<hr />
		<img src='http://avatar.ulfls.com/{$sid}.png?h=65&amp;w=80' alt='' style='float: right;'/>
		<label>{"Avatar"|loc}:</label>
		<input type='file' name='avatar'/><br />
		<label for='del_av'>{"Delete Picture"|loc}</label>
		<input type='checkbox' id='del_av' name='del_av' value='1'/><br />
	<hr />
		<input class='button' name='exit_button' type='submit' value='{"Save"|loc}' /><br />
		<input type='hidden' name='cval' value='$csum' />
	</form>

</section>
