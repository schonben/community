<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$extra_css[] = "postBase";

$pg_title = loc("Forum");

if ($setting['dev'])
$pg_toolbar = Array(
        Array(
            "url"=>"/editthread/{$id}",
            "onclick"=>"",
            "txt"=>"Edit Thread",
            "icon"=>"edit"
        ),
    );

$smarty->assign('thread',new thread($id));

?>
