<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$pg_title = loc("Dashboard");

$pg_toolbar = Array(
        Array(
            "url"=>"",
            "onclick"=>"showNewDashlet(this);",
            "txt"=>"New Dashlet",
            "icon"=>"dashlet"
        ),
    );


$row['left'] = Array();
$row['right'] = Array();

$counter['left'] = 0;
$counter['right'] = 0;

$area = "dashboard";			

$script = "";
$dashlets = dashlet::getDashlets($user,$area);

if (!count($dashlets)) {
    if (dashlet::populateDashlets($user))
        $dashlets = dashlet::getDashlets($user,$area);
}
 
foreach ($dashlets as $dashlet) {
    if ($dashlet->subpos) {
        $side = $dashlet->subpos;
    } else {
        if ($counter['right'] < $counter['left']) $side = "right"; else $side = "left";
    }
    $counter[$side] += $dashlet->size;
    $script .= $dashlet->script;
    $row[$side][] = <<<DASHLET
        <li id="{$dashlet->uniq}" class="box dashlet dashlet_{$dashlet->name}">
            {$dashlet->display()}
        </li>
DASHLET;
    $script .= $dashlet->getTimer();
}
$newdashlets = array();
foreach (glob($setting['apath']."/dashlets/*Dashlet.class.php") as $filename) {
    preg_match("|([^/]+)Dashlet.class.php|", $filename,$match);
    $newdashlets[] = $match[1];
}

$smarty->assign('newdashlets',$newdashlets);
$smarty->assign('script',$script);
$smarty->assign('row',$row);
$smarty->assign('sides',Array("left","right"));

?>
