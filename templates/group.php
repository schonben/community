<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$s=0;
$e=40;

$query = "
	select	id,
				title,
				description,
				tp
	from		groups
	where		id = '".pg_escape_string($_GET['id'])."'
	";
$gr_r = pg_fetch_array(pg_query($conn,$query));

$query = "
	select	t1.gid,
				t1.uid,
				t1.adm,
				t1.mod,
				t2.nick
	from		gu_link t1 left join
				users t2
	on			t1.uid = t2.id
	where		t1.gid = '".pg_escape_string($_GET['id'])."'
	";
$us = pg_query($conn,$query);

$query = "
	select	id,
				subject,
				posts,
				firstpost,
				lastpost,
				tags,
				0 as unread
	from
		threadtag
	left join
		read_threads
	on
		threadtag.id = read_threads.item_id
	and	$1 = read_threads.user_id
	where		id IN (
		select	thread
		from		thread_link
		where		guid = $2
		and		gr = true)
	order by	lastpost desc
	";
$params = Array($_SESSION['id'],$_GET['id']);
$po = pg_query_params($conn,$query,$params);


$pg_title = $gr_r['title'];

if (empty($_GET['page']))
	$_GET['page'] = 1;

$smarty->assign('title',$pg_title);
$smarty->assign('description',$gr_r['description']);
$smarty->assign('userlist',pg_fetch_all($us));
$smarty->assign('threadlist',thread_list($po,$_GET['page']));

?>