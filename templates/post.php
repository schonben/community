<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

//$extra_js[] = "js/jquery.bbcode.js";
$extra_css[] = "postBase";

$edit = "";
if (!empty($_REQUEST['reply'])) {
    // Reply to post
    $smarty->assign('action','reply');

    $pg_title = loc("Reply");

    $post = new post($_REQUEST['reply']);

} else if (!empty($id)) {
    // Edit post
    $smarty->assign('action','edit');
    
    $pg_title = loc("Edit Post");

    $post = new post($id);
    $edit = $post->body;
} else {
  	// New Post
    $smarty->assign('action','post');

    $pg_title = loc("New Post");

    $post = new post();


    $sql = "
		SELECT
			val,
			nr
		FROM
			#schema#.taglist
		WHERE
			val != 'general'
		ORDER BY
			val
			";
	$result = $db->query($sql);
    $smarty->assign('tags',$result->fetchAll());

    $sql = "
        SELECT
            uuid,
            'gr' as tp,
            title as short,
            description as long
        FROM
            #schema#.groups
    
        UNION
    
        SELECT
            uuid,
            'us' as tp,
            nick as short,
            fname || ' ' || lname as long
        FROM
            #schema#.users_info
        ORDER BY
            short
        ";
    $result = $db->query($sql);
    $data = array();
    while ($row = $result->fetch()) {
        $data[$row['uuid']] = array(
            "tp" => $row['tp'],
            "short" => $row['short'],
            "long" => $row['long'],
            );
    }
    $smarty->assign('tolist',$data);
    $smarty->assign('tolist_json',json_encode($data));
}


$smarty->assign('thread',new thread($post->thread));
$smarty->assign('post',$post);
$smarty->assign('title',$pg_title);
$smarty->assign('edit',$edit);

?>
