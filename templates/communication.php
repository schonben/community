<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$pg_title = loc("Communication");

if (empty($_GET['page']))
	$_GET['page'] = 1;

$pg_toolbar = Array(
		Array(
			"url"=>"/post",
			"txt"=>"New Post",
			"icon"=>"post"
		),
        Array(
            "url"=>"/markread.do?item=all&amp;tags={$id}",
            "txt"=>"Mark all as read",
            "icon"=>"mark"
        ),
	);

$threadlist = new threadlist($id);

//print_r($threadlist);

$smarty->assign('threadlist',$threadlist->showPage($_GET['page']));

?>