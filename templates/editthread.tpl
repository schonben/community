<script src="/js/post.js" type="text/javascript"></script>

<section class="page box">
    <form id="postform" action="/editthread.do" method="post">
        {include "threadinfo.tpl"}
        
        <div class="post_wrap">
            {$post->display()}
        </div>
        <input type="hidden" name="thread" value="{$thread->id}"/>
        <input type="submit" value="{"Save"|loc}"/>
    </form>
    <br class="clear"/>
</section>