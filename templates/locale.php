<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

if ($lvl < 5)
	return;

$lempty = array();
foreach ($setting['locs'] as $lc) {
	$lempty[$lc] = "";
}

$ll = Array();
// Normal
foreach ($locale_list as $key) {
	$ltmp = $lempty;
	if (!empty($loc[$key])) {
		foreach ($setting['locs'] as $lc) {
			if (isset($loc[$key][$lc]))
				$ltmp[$lc]=str_replace('\n',chr(13),$loc[$key][$lc]);
		}
	}
	if (strpos($key,"text"))
		$idx = 1;
	else
		$idx = 0;
	$ll[$idx][$key]=$ltmp;
}

$smarty->assign('locs',$setting['locs']);
$smarty->assign('ll',$ll);



?>