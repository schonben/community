<section class='columns'>
	{foreach $sides as $side}
		<div class='col {$side}'>
			{foreach $box.$side as $b}
				<div class='box'>
					<h2>{$b.header|loc}</h2>
					<table class='list'>
						{foreach $b.list as $nr=>$res}
							<tr>
								<td>{$nr}.</td>
								<td>{$res[0]}</td>
								<td>{$res[1]} ({$res[2]})</td>
							</tr>
						{/foreach}
					</table>
				</div>
			{/foreach}
		</div>
	{/foreach}
	<br class='clear'/>
</section>