{foreach $threads as $thread}
    <section class='box'>
        <h2><a href="thread/{$thread[0]->thread}">{$thread[0]->subject|local_format}</a></h2>
        {foreach $thread as $post}
            <div class='searchresult'>
                <h3>
                    <span>
                        Posted by <a href="/user/{$post->poster_id}" class="postedby">{$post->poster}</a>
                    </span>
                    <time datetime="{$post->dati->format("Y-m-d H:i:s")}">
                        <span class="date">{$post->dati->format("Y-m-d")}</span>
                        <span class="time">{$post->dati->format("H:i")}</span>
                    </time>
                </h3>
                {$post->body|local_format}
            </div>
        {/foreach}
    </section>
{/foreach}
