<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$pg_title = loc("Statistics");

$query = "
	select	nick,
				lvl,
				karma
	from		users
	order by	karma desc
	";
$km = pg_query($conn,$query);

$b = Array('header'=>"Top Karma",'list'=>Array());
for ($i=1;$i<11;$i++) {
	$km_r = pg_fetch_array($km);
	$b['list'][$i]=Array($km_r['karma'],$km_r['nick'],$km_r['lvl']);
}
$box['left'][] = $b;

$query = "
	select	nick,
				lvl,
				mp
	from		users
	order by	mp desc
	";
$mp = pg_query($conn,$query);

$b = Array('header'=>"Top Modpoints",'list'=>Array());
for ($i=1;$i<11;$i++) {
	$mp_r = pg_fetch_array($mp);
	$b['list'][$i]=Array($mp_r['mp'],$mp_r['nick'],$mp_r['lvl']);
}
$box['left'][] = $b;


$sql = "
    SELECT
        useragent as agent,
        COUNT(id) as num,
        SUM(hits) as hit
    FROM
        sitemon
    WHERE
        site = :site
    AND
        bot = FALSE
    AND
        hits > 1
    AND
       dati > :dati
    GROUP BY
        agent
	ORDER BY
	   agent
	";
$result = $db->prepare($sql);
$result->execute(array(":site"=>$site,":dati"=>date("Y-m-d H:i:s",time()-2592000)));

$browser = array();
$bt = 0;
$os = array();
$ot = 0;
while ($row = $result->fetch()) {
	$agent = uudb::agent($row['agent']);
	if (!empty($agent['browser'])) {
		if (!isset($browser[$agent['browser']]))
			$browser[$agent['browser']] = 0;
		$bt += $row['num'];
		$browser[$agent['browser']] += $row['num'];
	}
	if (!empty($agent['os'])) {
		if (!isset($os[$agent['os']]))
			$os[$agent['os']] = 0;
		$ot += $row['num'];
		$os[$agent['os']] += $row['num'];
	}
}
arsort($browser);
arsort($os);

$b = Array('header'=>"Top Browsers",'list'=>Array());
$i=0;
foreach ($browser as $brw => $nr) {
	$i++;
	$p = ($nr / $bt) * 100;
	if ($p > 10)
		$dp = number_format($p,0);
	else if ($p > 1)
		$dp = number_format($p,1);
	else
		$dp = number_format($p,2);
	$b['list'][$i]=Array($brw,'',$dp.'%');
}
$box['right'][] = $b;

$b = Array('header'=>"Top OS",'list'=>Array());
$i=0;
foreach ($os as $o => $nr) {
	$i++;			
	$p = ($nr / $ot) * 100;
	if ($p > 10)
		$dp = number_format($p,0);
	else if ($p > 1)
		$dp = number_format($p,1);
	else
		$dp = number_format($p,2);
	$b['list'][$i]=Array($o,'',$dp.'%');
}
$box['right'][] = $b;

$smarty->assign('sides',Array('left','right'));
$smarty->assign('box',$box);

?>