<section class="page box">
	<form action="localesave.run" method="post">
	<table style="width: 100%;">
		<tr>
			<th style="width: 1%;"></th>
			{foreach $locs as $lc}
				<th>{$lc}</th>
			{/foreach}
		</tr>
		{foreach $ll[0] as $key=>$l}
			<tr>
				<td style="font-size: .7em; vertical-align: top;">{$key}</td>
				{foreach $locs as $lc}
					<td>
						<input type="text" name="{$key}[{$lc}]" value="{$l.$lc}" style="width: 100%;"/>
					</td>
				{/foreach}
			</tr>
		{/foreach}
		{foreach $ll[1] as $key=>$l}
			<tr>
				<td style="font-size: .7em; vertical-align: top;">{$key}</td>
				{foreach $locs as $lc}
					<td>
						<textarea name="{$key}[{$lc}]" style="width: 100%;" rows="5">{$l.$lc}</textarea>
					</td>
				{/foreach}
			</tr>
		{/foreach}
		

		<tr>
			<td colspan="10">
				<input type="submit" value="{'Save'|loc}"/>
				<input type="submit" name="clean" value="{'Remove Empty'|loc}"/>
			</td>
		</tr>
	</table>
	</form>
</section>



