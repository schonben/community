<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

class threadlist {
    protected $records;
    protected $result;
    
    
    
    function __construct($tag=array()) {
        $query = "
            FROM
                #schema#.threads_tag
            LEFT JOIN
                #schema#.threads_read
            ON
                threads_tag.uuid = threads_read.item_id
            AND
                threads_read.user_id = :user
            LEFT JOIN
                #schema#.threads_link
            ON
                threads_tag.uuid = threads_link.thread
            AND
                threads_link.user = :user
            WHERE
                (threads_tag.private = false
            OR
                threads_link.thread is not null)
            ".static::tags($tag);
        $sql = "
            SELECT
                COUNT(threads_tag.uuid) as records
            {$query}
            ";
        $result = $GLOBALS['db']->prepare($sql);
        $result->execute(array(":user"=>$GLOBALS['user']->id));
        $row = $result->fetch();
        $this->records = $row['records'];
        
        $sql = "
            SELECT
                threads_tag.uuid as id,
                threads_tag.subject,
                threads_tag.posts,
                threads_tag.firstpost,
                threads_tag.lastpost,
                threads_tag.announce,
                threads_tag.arr_tags,
                COALESCE((threads_tag.posts - threads_read.rd),threads_tag.posts,0) as unread
            {$query}
            ORDER BY    
                threads_tag.lastpost desc
            OFFSET
                :offset
            LIMIT
                :limit
            ";
        $this->result = $GLOBALS['db']->prepare($sql);
    }

    static function tags ($tag=array()) {
        if (!is_array($tag)) {
            if (empty($tag))
                $tag = array();
            else
                $tag = explode(",",$tag);
        }
        $asql = ""; 
        foreach ($tag as $t)   
            $asql .= "and       '{$t}' = ANY (threads_tag.arr_tags)\n";

        $sql = "
            select
                tag_val
            from
                #schema#.marker_tag
            where
                user_id = :user
            and priority = -1
            ";
        $result = $GLOBALS['db']->prepare($sql);
        $result->execute(array(":user"=>$GLOBALS['user']->id));
        while ($row = $result->fetch()) {
            $asql .= "and not       '{$row['tag_val']}' = ANY (threads_tag.arr_tags)\n";
        }
        
        return $asql;
    }
    
    function showPage ($page=1, $lnr=30) {
        global $setting, $smarty;

        if ($page < 1) $page = 1;

        $this->result->execute(array(
            ":user"=>$GLOBALS['user']->id,
            ":offset"=>($page-1) * $lnr,
            ":limit"=>$lnr,
            ));
        
        
        $rows = Array();
        while ($row = $this->result->fetch(PDO::FETCH_ASSOC)) {
            if (!empty($row['unread'])) $ur = " unread"; else $ur = "";
            $fp = strtotime($row['firstpost']);
            $lp = strtotime($row['lastpost']);
            $rows[] = Array(
                'id'=>$row['id'],
                'fp'=>date("Y-m-d H:i",$fp),
                'lp'=>date("Y-m-d H:i",$lp),
                'tags'=>$row['tags'],
                'subject'=>$row['subject'],
                'unread'=>$row['unread'],
                'posts'=>$row['posts'],
                'announce'=>$row['announce'],
                );
        }

        $smarty->assign('page',$page);
        $smarty->assign('tag',"");
        $smarty->assign('total',ceil($this->records / $lnr));
        $smarty->assign('rows',$rows);
    
        return $smarty->fetch('threadlist.tpl');
    }
}

?>