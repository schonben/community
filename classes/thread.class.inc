<?php

class thread {
    protected $id;
    protected $subject;
    protected $private=false;
    protected $announce=false;
    protected $tags = array();
    protected $idx = 0;
    protected $unread=0;
    
    function __construct ($id) {
        if (!empty($id)) {
            $this->retrieve($id);
        }
    }
    function __get ($name) {
        switch ($name) {
            default:
                if (isset($this->$name))
                    return $this->$name;
            break;
        }
    }
    function __set($name,$value) {
        switch ($name) {
            // TODO fix with real stuff
            case "level":
                $this->$name = $value;
            break;
        }
    }
    function retrieve ($id) {
        $sql = "
            select
                *
            from
                #schema#.threads_tag
            where
                uuid = :thread
            ";
        $thread = $GLOBALS['db']->prepare($sql);
        $thread->execute(array(":thread"=>$id));
        while ($row = $thread->fetch()) {
            $this->id = $row['uuid'];
            $this->subject = $row['subject'];
            $this->private = $row['private'];
            $this->announce = $row['announce'];
            if (is_array($row['tags']))
                $this->tags = $row['tags'];
        }
        if (empty($this->id)) {
            throw new Exception("Invalid ID", 1);
        }
        $sql = "
            SELECT
                idx,
                true_idx
            FROM
                #schema#.marker_read
            WHERE
                item_id = :item
            AND
                user_id = :user
            ";
        $marker = $GLOBALS['db']->prepare($sql);
        $marker->execute(array(":item"=>$this->id,":user"=>$GLOBALS['user']->id));
        while ($row = $marker->fetch()) {
            if ($row['idx'] > $this->idx)
                $this->idx = $row['idx'];
        }
    }

    function getId() {
        return $this->id;
    }
    function getIdx() {
        return $this->idx;
    }
    function getTags() {
        return $this->tags;
    }
    function getSubject() {
        return $this->subject;
    }
    function getLinks($rows=true) {
        $sql = "
            SELECT
                *
            FROM
                #schema#.threads_link_name link
            WHERE
                thread_id = :thread
            ORDER BY
                link.name
            ";
        $result = $GLOBALS['db']->prepare($sql);
        $result->execute(array(":thread"=>$this->id));
        $data = array();
        while ($row = $result->fetch()) {
            if ($rows) {
                $data[] = $row;
            } else {
                $data[] = $row['uuid'];
            }
        }
        return $data;
    }

    function getPost($action="first") {
        $sql = "
            SELECT  
                uuid,
                thread_id,
                parent_id,
                user_id,
                idx
            FROM
                #schema#.posts
            WHERE
                parent_id IS NULL
            AND
                thread_id = :thread
            ORDER BY
                dati
            LIMIT 1
            ";
        $posts = $GLOBALS['db']->prepare($sql);
        $posts->execute(array(
            ":thread" => $this->id,
            ));
        while ($row = $posts->fetch()) {
            return $row['uuid'];
        }
        return false;
    }
    
    function getThread ($parent=NULL,$level=0) {        
        $sql = "
            SELECT  
                uuid,
                thread_id,
                parent_id,
                user_id,
                poster,
                sex,
                dati,
                body,
                idx,
                sig
            FROM
                #schema#.threads_posts
            WHERE
                thread_id = :thread
            ORDER BY
                dati
            ";
        $posts = $GLOBALS['db']->prepare($sql);
        $posts->execute(array(
            ":thread" => $this->id,
            ));

        $p = Array();
        $idx = 0;
        while ($row = $posts->fetch()) {
            if ($row['idx'] > $idx) $idx = $row['idx'];
            $p[$row['parent_id']][] = new post($row,$this->idx);
        }
        return $this->sortArray($p);
    }
    private function sortArray(&$p,$parent="",$level=0) {
        $return = array();
        foreach ($p[$parent] as &$post) {
            $post->level = $level;
            $return[] = $post;
            if (isset($p[$post->id])) {
                $return = array_merge($return,$this->sortArray($p,$post->id,$level+1));
            }
        }
        return $return;
    }
    
    function save () {
        $record = array(
                ":uuid" => $this->id,
                ":subject" => $this->subject,
                ":private" => $this->private,
                ":announce" => $this->announce,
                );
        if (empty($this->id)) {
            // New record
            $sql = "
                INSERT INTO
                    #schema#.threads (
                        uuid,
                        subject,
                        private,
                        announce,
                        tp
                    ) VALUES (
                        :uuid,
                        :subject,
                        :private,
                        :announce,
                        :tp
                    )
                ";
            $query = $GLOBALS['db']->prepare($sql);
            $query->execute($record);
        
        } else {
            // Update
            $sql = "
                UPDATE
                    #schema#.threads
                SET
                    subject = :subject,
                    private = :private,
                    announce = :announce,
                    tp = :tp
                WHERE
                    uuid = :uuid
                ";
            $query = $GLOBALS['db']->prepare($sql);
            $query->execute($record);
        }
    }
}

?>