<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

abstract class dashlet {
	protected $smarty;
    protected $area;
    protected $subpos;
	protected $data = array();
	protected $script = "";
    protected $extra_js = array();
    protected $uniq;
    protected $setting = array();

	// Settings
	protected $updTime = 0;
	protected $size = 1;
	protected $template = "standard";
    protected $name = "unnamed";
	protected $title = "Unnamed Dashlet";
	// End Settings

	function __get($name) {
		switch ($name) {
			default:
				if (isset($this->$name))
					return $this->$name;
			break;
		}
	}
	
	function __construct ($area,$uniq="") {
        if (empty($uniq))    
            $this->uniq = uniqid();
        else
            $this->uniq = $uniq;
		$this->smarty = clone $GLOBALS['smartyBase'];
        $this->area = $area;
		$this->main();
	}	

	abstract function main ();	
	
	public function display() {
		$this->smarty->assign("title",$this->title);
		$this->smarty->assign("data",$this->data);
        $this->smarty->assign("dashlet",$this);
		
		return $this->smarty->fetch("dashlet_{$this->template}.tpl");
	}
    public function getTimer() {
        if ($this->updTime) {    
            $timer = $this->updTime * 1000;  
            return "     setInterval(\"loadDashlet('{$this->name}','$this->uniq','{$this->area}')\",{$timer});\n";
                }
    }
    public function importSettings($settings) {
        if (!is_array($settings))
            $settings = json_decode($settings);
        foreach ($settings as $name=>$value) {
            if (isset($this->setting['name']))
                $this->setting['name'] = $value;
        }
    }
    public function exportSettings() {
        return json_encode($this->setting);
    }
    public function setSubPos($subpos) {
        $this->subpos = $subpos;
    }

    static function getDashlets ($user,$area) {
        $sql = "
            SELECT
                uuid,
                dashlet,
                subpos,
                settings
            FROM
                #schema#.dashlets
            WHERE
                pos = :area
            AND
                user_id = :user
            ORDER BY
                so
            ";
        $da = $GLOBALS['db']->prepare($sql);
        $da->execute(array(":area"=>$area,":user"=>$user->id));
        
        $dash = array();
        while ($row = $da->fetch()) {
            $id = $row['dashlet'];
            $class = $id."Dashlet";
            if (classloader($class,"dashlets")) {
                $dashlet = new $class($area,$row['uuid']);
                $dashlet->setSubPos($row['subpos']);
                if (!empty($row['settings'])) {
                    $dashlet->importSettings($row['settings']);
                }
                $GLOBALS['extra_js'] = array_merge($GLOBALS['extra_js'],$dashlet->extra_js);
                $dash[] = $dashlet;
            } else if ($GLOBALS['setting']['dev']) {
                error_log("Dashlet class: {$class} missing");
            }
        }
        return $dash;
    }

    static function populateDashlets($user) {
        $sql = "
            SELECT
                count(dashlet) as nr
            FROM
                #schema#.dashlets
            WHERE
                user_id = :user
            ";
        $result = $GLOBALS['db']->prepare($sql);
        $result->execute(array(":user"=>$user->id));
        $row = $result->fetch();
        if ($row['nr'] == 0) {
            $sql = "
                INSERT INTO
                    #schema#.dashlets (
                        user_id,
                        dashlet,
                        pos,
                        so
                    ) VALUES (
                        :user,
                        :dashlet,
                        :pos,
                        :so
                    )
                ";
            $query = $GLOBALS['db']->prepare($sql);
            foreach ($GLOBALS['setting']['dashlets'] as $pos => $dashlets) {
                $so = 0;
                foreach ($dashlets as $dashlet) {
                    $so += 2;
                    $query->execute(array(
                        ":user"=>$user->id,
                        ":dashlet"=>$dashlet,
                        ":pos"=>$pos,
                        ":so"=>$so,
                        ));
                }
            }
            return true;
        }
        return false;
    }

}

?>