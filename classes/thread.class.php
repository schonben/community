<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

class thread {
    protected $id;
    protected $subject;
    protected $private=false;
    protected $announce=false;
    protected $tags = array();
    protected $recipients = array();
    protected $idx = 0;
    protected $unread=0;
    protected $new=true;
    
    function __construct ($id) {
        if (!empty($id)) {
            $this->retrieve($id);
        } else {
            $this->id = gen_uuid();
            $this->updateIdx();
        }
    }
    
    function __get ($name) {
        switch ($name) {
            default:
                if (isset($this->$name))
                    return $this->$name;
            break;
        }
    }
    function __set($name,$value) {
        switch ($name) {
            // TODO fix with real stuff
            case "tags":
            case "recipients":
                if (is_array($value)) {
                    $this->$name = $value;
                } else if (empty($value)) {
                    $this->$name = array();
                } else {
                    $this->$name = explode(",",$value);
                }
            break;
            case "subject":
                $this->$name = $value;
            break;
            case "private":
            case "announce":
                $this->$name = $value;
            break;
        }
    }

    function getId() {
        return $this->id;
    }
    function getIdx() {
        return $this->idx;
    }
    function getTags() {
        return $this->tags;
    }
    function getSubject() {
        return $this->subject;
    }

    function getLinks($rows=true) {
        $sql = "
            SELECT
                *
            FROM
                #schema#.threads_link_name link
            WHERE
                thread_id = :thread
            ORDER BY
                link.name
            ";
        $result = $GLOBALS['db']->prepare($sql);
        $result->execute(array(":thread"=>$this->id));
        $data = array();
        while ($row = $result->fetch()) {
            if ($rows) {
                $data[] = $row;
            } else {
                $data[] = $row['uuid'];
            }
        }
        return $data;
    }

    function getPost($action="first") {
        $sql = "
            SELECT  
                uuid,
                thread_id,
                parent_id,
                user_id,
                idx
            FROM
                #schema#.posts
            WHERE
                parent_id IS NULL
            AND
                thread_id = :thread
            ORDER BY
                dati
            LIMIT 1
            ";
        $posts = $GLOBALS['db']->prepare($sql);
        $posts->execute(array(
            ":thread" => $this->id,
            ));
        while ($row = $posts->fetch()) {
            return $row['uuid'];
        }
        return false;
    }
    
    function getThread ($parent=NULL,$level=0) {        
        $sql = "
            SELECT  
                uuid,
                thread_id,
                parent_id,
                user_id,
                poster,
                sex,
                dati,
                body,
                idx,
                sig
            FROM
                #schema#.threads_posts
            WHERE
                thread_id = :thread
            ORDER BY
                dati
            ";
        $posts = $GLOBALS['db']->prepare($sql);
        $posts->execute(array(
            ":thread" => $this->id,
            ));

        $p = Array();
        $idx = 0;
        while ($row = $posts->fetch()) {
            if ($row['idx'] > $idx) $idx = $row['idx'];
            $p[$row['parent_id']][] = new post($row,$this->idx);
        }
        return $this->sortArray($p);
    }

    function addTag($tag) {
        if (!in_array($tag, $this->tags))
            $this->tags[] = $tag;
    }

    function retrieve ($id) {
        $sql = "
            select
                *
            from
                #schema#.threads_tag
            where
                uuid = :thread
            ";
        $thread = $GLOBALS['db']->prepare($sql);
        $thread->execute(array(":thread"=>$id));
        while ($row = $thread->fetch()) {
            $this->new = false;
            $this->id = $row['uuid'];
            $this->subject = $row['subject'];
            $this->private = $row['private'];
            $this->announce = $row['announce'];
            if (is_array($row['tags']))
                $this->tags = $row['tags'];
        }
        if (empty($this->id)) {
            throw new Exception("Invalid ID", 1);
        }
        $this->updateIdx();
    }

    protected function updateIdx () {
        $sql = "
            SELECT
                idx,
                true_idx
            FROM
                #schema#.marker_read
            WHERE
                item_id = :item
            AND
                user_id = :user
            ";
        $marker = $GLOBALS['db']->prepare($sql);
        $marker->execute(array(":item"=>$this->id,":user"=>$GLOBALS['user']->id));
        while ($row = $marker->fetch()) {
            if ($row['idx'] > $this->idx)
                $this->idx = $row['idx'];
        }
    }

    private function sortArray(&$p,$parent="",$level=0) {
        $return = array();
        foreach ($p[$parent] as &$post) {
            $post->level = $level;
            $return[] = $post;
            if (isset($p[$post->id])) {
                $return = array_merge($return,$this->sortArray($p,$post->id,$level+1));
            }
        }
        return $return;
    }
    
    function save () {
        $record = array(
                ":uuid" => $this->id,
                ":subject" => $this->subject,
                ":private" => $this->private?"true":"false",
                ":announce" => $this->announce?"true":"false",
                ":tp" => "f",
                );
        if ($this->new) {
            // New record
            $sql = "
                INSERT INTO
                    #schema#.threads (
                        uuid,
                        subject,
                        private,
                        announce,
                        tp
                    ) VALUES (
                        :uuid,
                        :subject,
                        :private,
                        :announce,
                        :tp
                    )
                ";
            $query = $GLOBALS['db']->prepare($sql);
            $query->execute($record);
        } else {
            // Update
            $sql = "
                UPDATE
                    #schema#.threads
                SET
                    subject = :subject,
                    private = :private,
                    announce = :announce,
                    tp = :tp
                WHERE
                    uuid = :uuid
                ";
            $query = $GLOBALS['db']->prepare($sql);
            $query->execute($record);
        }
        
        $this->updateRecipients();
        $this->updateTags();
        
    }
    function updateRecipients () {
        // Remove old recipients
        $sql = "
            DELETE
            FROM
                #schema#.thread_link
            WHERE
                thread_id = :uuid
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":uuid"=>$this->id));
        
        // Insert recipients
        $sql = "
            INSERT INTO
                #schema#.thread_link (
                    uuid,
                    thread_id
                ) values (
                    :uuid,
                    :thread
                )";
        $query = $GLOBALS['db']->prepare($sql);

        if ($this->private && !in_array($GLOBALS['user']->id, $this->recipients)) {
            $this->recipients[] = $GLOBALS['user']->id;
        }
        
        foreach ($this->recipients as $recipient) {
            if (is_uuid($recipient)) {
                $query->execute(array(
                    ":thread"=>$this->id,
                    ":uuid"=>$recipient,
                    ));
            }
        }
    }
    function updateTags() {
        // Remove old tags
        $sql = "
            DELETE
            FROM
                #schema#.tags
            WHERE
                uuid = :uuid
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":uuid"=>$this->id));
                
        // Insert tags
        $sql = "
            INSERT INTO
                #schema#.tags (
                    uuid,
                    val
                ) values (
                    :uuid,
                    :tag
                )";
        $query = $GLOBALS['db']->prepare($sql);
        foreach ($this->tags as $tag) {
            $query->execute(array(
                ":uuid"=>$this->id,
                ":tag"=>$tag,
                ));
        }
    }
}

?>