<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

class post {
    protected $id;
    protected $level;
    protected $thread;
    protected $parent;
    protected $poster_id;
    protected $poster;
    protected $dati;
    protected $subject;
    protected $body;
    protected $idx;
    protected $sig;
    protected $sex;
    
    protected $unread=false;
    protected $tools=array();
    
    function __construct ($id="",$idx=false) {
        if (!empty($id)) {
            $this->retrieve($id,$idx);
        }
        $this->tools['reply'] = array('link'=>'/post?reply='.$this->id,'icon'=>'/layout/'.$GLOBALS['setting']['layout'].'/images/icons/reply.png');
        if ($this->poster_id == $GLOBALS['user']->id || $GLOBALS['user']->lvl >= 10)
            $this->tools['edit'] = Array('link'=>'/post/'.$this->id,'icon'=>'/layout/'.$GLOBALS['setting']['layout'].'/images/icons/edit.png');
        
    }
    
    function __get ($name) {
        switch ($name) {
            case "subject":
                if (empty($this->subject) && !empty($this->id))
                    $this->subject = $this->getSubject();
                return $this->subject;
            break;
            default:
                if (isset($this->$name))
                    return $this->$name;
            break;
        }
    }
    function __set($name,$value) {
        switch ($name) {
            case "level":
                $this->$name = $value;
            break;
        }
    }
    
    function getSubject() {
        if (empty($GLOBALS['db']->results['subject'])) {
            $sql = "
                SELECT
                    subject
                FROM
                    #schema#.threads
                WHERE
                    uuid = :thread
                    ";
            $GLOBALS['db']->results['subject'] = $GLOBALS['db']->prepare($sql);
        }
        $GLOBALS['db']->results['subject']->execute(array(":thread"=>$this->thread));
        while ($row = $GLOBALS['db']->results['subject']->fetch()) {
            return $row['subject'];
        }
        return false;
    }
    
    function retrieve ($id,$idx=false) {
        if (is_array($id)) {
            $row = $id;
        } else {
            if (empty($GLOBALS['db']->results['posts'])) {
                $sql = "
                    SELECT
                        uuid,
                        thread_id,
                        parent_id,
                        user_id,
                        poster,
                        sex,
                        dati,
                        body,
                        idx,
                        sig
                    FROM
                        #schema#.threads_posts
                    WHERE
                        uuid = :uuid
                    ";
                $GLOBALS['db']->results['posts'] = $GLOBALS['db']->prepare($sql);
            }
            $GLOBALS['db']->results['posts']->execute(array(":uuid"=>$id));
            $row = $GLOBALS['db']->results['posts']->fetch();
        }
        $this->id = $row['uuid'];
        $this->level = 0;
        $this->thread = $row['thread_id'];
        $this->parent = $row['parent_id'];
        $this->poster_id = $row['user_id'];
        $this->poster = $row['poster'];
        $this->dati = new DateTime($row['dati']);
        $this->subject = "";
        $this->body = $row['body'];
        $this->idx = $row['idx'];
        $this->sig = $row['sig'];
        $this->sex = $row['sex'];
        
        $this->unread = ($idx < $row['idx'] && $idx !== false)?true:false;
        $this->tools = array();
    }

    function display () {
        $smarty = clone $GLOBALS['smartyBase'];
        
        $smarty->assign("post",$this);
        return $smarty->fetch("postTemplate.tpl");
    }
    function save () {
        return "Test";
    }
}

?>