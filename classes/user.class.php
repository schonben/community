<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

class user extends uudbuser {
	protected $lvl;

	protected $login;
	protected $nick;

	function __construct ($uuid = "", $heartbeat=false, $action = true) {
        parent::__construct($uuid);
		if (!empty($this->id) && $heartbeat) {
			$this->heartbeat($action);
		}
	}

    function populate ($id) {
        $row = parent::populate($id);

        if (empty($row['uuid'])) {
            // User don't exist in community
            
            $nick = $row['fname'];
            $sql = "
                INSERT INTO 
                    #schema#.users (
                        uuid,
                        nick
                    ) VALUES (
                        :uuid,
                        :nick
                    )
                ";
            $query = $this->db->prepare($sql);
            $query->execute(array(":uuid" => $id,":nick" => $nick));

            $this->lvl = 0;
            $this->login = $row['email'];
            $this->nick = $nick;
        } else {
            $this->lvl = $row['lvl'];
            $this->login = $row['email'];
            $this->nick = $row['nick'];
        }

        
        return $row;
    }

	function heartbeat ($action=true) {
		$sql = "
			UPDATE
				#schema#.users
			SET
				last_heartbeat = now()
			".($action ? ",last_action = now()": "")."
			WHERE
				uuid = :uuid
				";
		$hb = $this->db->prepare($sql);
		$params = array(":uuid"=>$this->id);
		$hb->execute($params);
	}

	function displayName() {
		return $this->nick;
	}

}

?>