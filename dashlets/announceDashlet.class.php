<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

class announceDashlet extends dashlet {

    protected $updTime = 60;
    protected $size = 3;
    protected $template = "announce";
    protected $name = "announce";
    protected $title = "Announcements";

    function main () {
        $sql = "
            SELECT
                threads_tag.uuid as id,
                threads_tag.subject,
                threads_tag.posts,
                threads_tag.firstpost,
                threads_tag.lastpost,
                threads_tag.announce,
                threads_tag.arr_tags,
                post.uuid as post_id,
                post.body,
                post.user_id,
                post.poster,
                COALESCE((threads_tag.posts - threads_read.rd),threads_tag.posts,0) as unread
            FROM
                #schema#.threads_tag
            LEFT JOIN
                #schema#.threads_read
            ON
                threads_tag.uuid = threads_read.item_id
            AND
                threads_read.user_id = :user
            LEFT JOIN
                #schema#.threads_link
            ON
                threads_tag.uuid = threads_link.thread
            AND
                threads_link.user = :user
            LEFT JOIN
                #schema#.post
            ON
                threads_tag.uuid = post.thread_id
            AND
                post.parent_id IS NULL
            WHERE
                threads_tag.announce = true
            AND
                (threads_tag.private = false
            OR
                threads_link.thread is not null)
            ".threadlist::tags()."
            ORDER BY    
                threads_tag.lastpost desc
            LIMIT :limit
            ";
        $result = $GLOBALS['db']->prepare($sql);
        $result->execute(array(":user"=>$GLOBALS['user']->id,":limit"=>6));
                    
        $this->data = Array();
        while ($row = $result->fetch()) {
            $this->data[] = Array(
                "link" => "/thread/{$row['id']}?r=".rand(10,99)."#unread0",
                "links" => Array(0),
                "class" => ($row['unread']?"unread":""),
                "subject" => local_format($row['subject']),
                "unread" => $row['unread'],
                "posts" => $row['posts'],
                "tags" => $row['tags'],
                "dati" => $row['firstpost'],
                "dat" => date("d.m.Y",strtotime($row['firstpost'])),
                "tim" => date("H:i",strtotime($row['firstpost'])),
                "user_id" => $row['user_id'],
                "poster" => $row['poster'],
                "body" => get_lead(preg_replace("|\[img\][^[]+\[/img\]|i","",$row['body']),500),
                );  
        }
    }
}

?>
