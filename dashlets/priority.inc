<?php

// Init

$upd_time = 15;
$size = 1;
$dev = true;

//

$asql = "";

$query = "
	select
		tag_val
	from
		$setting[schema].tag_marker
	where
		user_id = $1
	and	priority = 1
	";
$param = Array($_SESSION[id]);
$tm = pg_query_params($conn,$query,$params);
while ($tm_r = pg_fetch_array($tm)) {
	$asql .= "and 		'$tm_r[tag_val]' = ANY (threadtag.tags)\n";	
}


$query = "
	select
		threadtag.id,
		threadtag.subject,
		threadtag.posts,
		threadtag.firstpost,
		threadtag.lastpost,
		threadtag.tags,
		COALESCE((threadtag.posts - read_threads.rd),threadtag.posts,0) as unread
	from
		$setting[schema].threadtag
	left join
		$setting[schema].read_threads
	on
		threadtag.id = read_threads.item_id
	and	$1 = read_threads.user_id
	where
		threadtag.tp = 'fo'
	and	(threadtag.private = false
	or			threadtag.id IN (
		select
			thread
		from
			$setting[schema].th_link
		where
			uid = $1))
	$asql
	order by	
		threadtag.lastpost desc
	";
$param = Array($_SESSION[id]);
$pr = pg_query_params($conn,$query,$params);

$size = 1+floor(pg_num_rows($pr) / 5);

$info_out = "
	<h2>".loc("Priority Posts")."</h2>
	";
$info_out .= "
	<p>	
	";
while ($on_r = pg_fetch_array($on)) {
	$info_out .= "<a href='user_$on_r[id].html'>$on_r[nick]</a> (<span id='$rid'>$dff</span>)<br/>";
}
$info_out .= "</p>";

?>
