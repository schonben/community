<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

class bdayDashlet extends dashlet {

	protected $updTime = 0;
	protected $size = 1;
    protected $name = "bday";
	protected $title = "Birthdays";

	private $days = 28;

	function main () {
		$s = date("z")+1;
		$diy = 365+date("L");
		$ma = date("m");
		$mb = date("m",time()+(60*60*24*$this->days));
		if ($mb < $ma)
			$mb += 12;
		$da = date("d");
		$db = date("d",time()+(60*60*24*$this->days));
		
		$sql = "
			select
				nick as showname,	
				birthdate,
				uuid,
				extract(doy from birthdate) as doy,
				extract(month from birthdate) as month,
				extract(day from birthdate) as day,
				age(birthdate - {$this->days}) as age,
				sm
			from
				#schema#.users_info
			where
				lvl > 2
			and		(sm > {$ma}
			or			(sm = {$ma}
			and		extract(day from birthdate) >= {$da}))
			and		(sm < {$mb}
			or			(sm = {$mb}
			and		extract(day from birthdate) <= {$db}))
			order by	sm, extract(day from birthdate) 
			";
        $bd = $GLOBALS['db']->prepare($sql);
        $bd->execute();

		$data = Array();
		while ($bd_r = $bd->fetch()) {
		//	if ($bd_r[doy] < $s)
		//		$bd_r[doy] += $diy;
			if ($bd_r['sm'] > 12)
				$ts = mktime(0,0,0,$bd_r['month'],$bd_r['day'],date("Y")+1);
			else
				$ts = mktime(0,0,0,$bd_r['month'],$bd_r['day'],date("Y"));
		
			$ds = round(($ts - mktime(0,0,0,date("m"),date("d"),date("Y"))) / (60*60*24));
						
			switch ($ds) {
				case 0:
					$dat = "Today";
				break;
				case 1:
					$dat = "Tomorrow";
				break;
				case 2:
					$dat = "Day after tomorrow";
				break;
				default:
					$dat = "$bd_r[day] ".loc("month_$bd_r[month]");
				break;
			}
			$a = explode(" ",$bd_r['age']);
			$this->data[] = Array(
				"link" => "/user/{$bd_r['uuid']}",
				"links" => Array(1),
				"class" => "",
				"cols" => Array(
					$dat,
					$bd_r['showname'],
					"($a[0] ".loc("years").")",
					),
				);	
		}
		
		if (!count($this->data))
			$this->data[] = Array(
				"link" => "",
				"class" => "",
				"cols" => Array(
					"No birthday in $this->days days.",
					),
				);
		
		
	}


	
}

?>