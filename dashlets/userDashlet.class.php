<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

class userDashlet extends dashlet {

	protected $updTime = 15;
	protected $size = 1;
    protected $name = "user";
	protected $title = "Users Online";

	function main () {
		$this->extra_js[] = "js/dashlet_user.js";
		
		$sql = "
			select
				id,
				uuid,
				nick,
				karma,
				date_part('epoch',now()) as ref, 
				date_part('epoch', last_action) as action
			from
				#schema#.users
			where
				date_part('epoch', last_heartbeat) > date_part('epoch',now()) - 90
			order by
				last_action desc
			";
		$on = $GLOBALS['db']->prepare($sql);
		$on->execute();
		
		
		$ridl = Array();
		$rand = rand(10000,99999);
        $size = 0;
		while ($on_r = $on->fetch()) {
		    $size++;
			$rid = $on_r['id']."_".$this->area;
			$ridl[] = $rid;
			$diff = $on_r['ref'] - $on_r['action'];
			$this->data[] = Array(
				"link" => "/user/{$on_r['uuid']}",
				"links" => Array(0),
				"class" => "",
				"cols" => Array(
					$on_r['nick'],
					"(<span id='$rid'>{$this->dispDiff($diff)}</span>)",
					),
				);	
		
		}
        $this->size = ceil($size / 5);
		
		$this->script .= "
			setInterval(\"updTimer('".implode(",",$ridl)."')\",1000);
			";

	}
	function dispDiff($diff) {
		if ($diff < 90) {
			return round($diff)." secs";
		} else if ($diff < 5400) {
			return round($diff / 60,1)." mins";
		} else if ($diff < 129600) {
			return round($diff / 3600,1)." hours";
		} else {
			return round($diff / 86400,1)."days";
		}
	}
}

?>
