<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

class unreadDashlet extends dashlet {
    protected $updTime = 10;
    protected $size = 1;
    protected $name = "unread";
    protected $title = "Unread Posts";

    function main() {
        $sql = "
            SELECT
                threads_tag.uuid as id,
                threads_tag.subject,
                threads_tag.posts,
                threads_tag.firstpost,
                threads_tag.lastpost,
                threads_tag.announce,
                threads_tag.arr_tags,
                COALESCE((threads_tag.posts - threads_read.rd),threads_tag.posts,0) as unread
            FROM
                #schema#.threads_tag
            LEFT JOIN
                #schema#.threads_read
            ON
                threads_tag.uuid = threads_read.item_id
            AND
                threads_read.user_id = :user
            LEFT JOIN
                #schema#.threads_link
            ON
                threads_tag.uuid = threads_link.thread
            AND
                threads_link.user = :user
            WHERE
                (threads_tag.private = false
            OR
                threads_link.thread is not null)
            AND
                COALESCE((threads_tag.posts - threads_read.rd),threads_tag.posts,0) > 0
            ".threadlist::tags()."
            ORDER BY    
                threads_tag.lastpost desc
            LIMIT
                :limit
            ";
        $result = $GLOBALS['db']->prepare($sql);
        $result->execute(array(":user"=>$GLOBALS['user']->id,":limit"=>15));
        while ($row = $result->fetch()) {
            $this->data[] = Array(
                "link" => "/thread/{$row['id']}?r=".rand(10,99)."#unread0",
                "links" => Array(0),
                "class" => "unread",
                "cols" => Array(
                    local_format($row['subject']),
                    $row['unread'],
                    $row['posts'],
                    ),
                );  
        }
        if (!count($this->data))
            $this->data[] = Array(
                "link" => "",
                "class" => "",
                "cols" => Array(
                    "No unread messages.",
                    ),
                );
        

    }
}

?>
