<?php

if (!defined("ENTRY") || ENTRY != "post")
    exit("Invalid entry point");

$loct = array();
foreach ($setting['locs'] as $lc) {
	$loct[$lc] = "";
}
foreach ($locale_list as $key) {
	foreach ($setting['locs'] as $lc) {
		if (!empty($_POST[$key][$lc])) {
			$t = str_replace(chr(10),'',$_POST[$key][$lc]);
			$t = str_replace(chr(13),'\n',$t);
			$loct[$lc] .= '	$loc["'.$key.'"]["'.$lc.'"] = '."'".$t."';\n";
		}
	}
}

$text = "";
foreach ($setting['locs'] as $lc) {
	$text .= $loct[$lc];
}


$page = '<?php

function poploc ($locale) {
'.$text.'
	return $loc;
}

?>';

file_put_contents($setting['localepath']."/locale.inc", $page);

if (isset($_POST['clean'])) {
	$llist = Array();
	foreach ($loc as $key => $val) {
		$llist[] = $key;
	}
	writeloclist($llist);
	$ret_url = "?tmpl=locale";
}

?>
