<?php

if (!defined("ENTRY") || ENTRY != "post")
    exit("Invalid entry point");

$thread = new thread($_POST['thread']);
$thread->private = !empty($_POST['private']);
$thread->announce = !empty($_POST['announce']);

if (empty($_POST['tags'])) {
    if ($_POST['private'] == "false")    
        $_POST['tags'] = "general";
    else
        $_POST['tags'] = "private";
}
$thread->tags = $_POST['tags'];

if (empty($_POST['recipients']))
    $thread->recipients = array();
else
    $thread->recipients = explode(",",$_POST['recipients']);

$thread->subject = $_POST['subject'];

$thread->save();

$returl = "thread/{$thread->id}#unread0";

?>