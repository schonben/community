<?php

if (!defined("ENTRY") || ENTRY != "post")
    exit("Invalid entry point");

$query = "
	update
		users
	set
		nick = $2,
		locale = $3,
		sig = $4
	where
		id = $1
	";
$params = Array($_SESSION['id'],$_POST['nick'],$_POST['set_locale'],$_POST['sig']);
pg_query_params($conn, $query, $params);

if ($_FILES['avatar']['tmp_name']) {
	move_uploaded_file($_FILES['avatar']['tmp_name'], $setting[avatarpath]."/".$_SESSION['id']);
}

?>