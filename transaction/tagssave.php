<?php

if (!defined("ENTRY") || ENTRY != "post")
    exit("Invalid entry point");

$query = "
	select
	 	taglist.val,
	 	taglist.nr,
	 	tag_marker.priority
	from
		$setting[schema].taglist
	left join
		$setting[schema].tag_marker
	on
		taglist.val = tag_marker.tag_val
	and	tag_marker.user_id = $1
	order by	taglist.nr desc, taglist.val
	";
$params = Array($_SESSION['id']);
$tg = pg_query_params($conn,$query,$params);

$taglist = Array();
while ($tg_r = pg_fetch_array($tg)) {
	$taglist[] = $tg_r['val'];
	if (isset($_POST['tag'][$tg_r['val']]) && $_POST['tag'][$tg_r['val']] > -2 && $_POST['tag'][$tg_r['val']] < 2) {
		if ($tg_r['priority'] > -2 && $tg_r['priority'] < 2) {
			$query = "
				update
					$setting[schema].tag_marker
				set
					priority = $1
				where
					tag_val = $2
				and	user_id = $3
				";
			$params = Array($_POST['tag'][$tg_r['val']],$tg_r['val'],$_SESSION['id']);
			pg_query_params($conn,$query,$params);
		} else {
			$query = "
				insert into
					$setting[schema].tag_marker (
						priority,
						tag_val,
						user_id
					) values (
						$1,
						$2,
						$3
					)
				";
			$params = Array($_POST['tag'][$tg_r['val']],$tg_r['val'],$_SESSION['id']);
			pg_query_params($conn,$query,$params);
		}
	}
}
$query = "
	delete
	from
		$setting[schema].tag_marker
	where
		user_id = $1
	and	tag_val NOT IN ('".implode("','",$taglist)."')
	";
$params = Array($_SESSION['id']);
pg_query_params($conn,$query,$params);


?>