<?php

$tmp = $_SESSION['uuid'];
$_SESSION['uuid']="";

if (!empty($_POST['assertion'])) {
	$url = "https://browserid.org/verify";
	$postdata='assertion='.urlencode($_POST['assertion']).'&audience='.urlencode($_SERVER['HTTP_HOST']);
	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt ($ch, CURLOPT_POSTFIELDS, $postdata);
	curl_setopt ($ch, CURLOPT_POST, 1);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	$return = json_decode(curl_exec($ch));
	if ($return->status == "okay" && $return->expires > time()) {
		$_SESSION['email'] = $return->email;
		$query = "
			SELECT
				id
			FROM
				users
			WHERE
				email = $1
			";
		$us = pg_query_params($conn,$query,array($return->email));
		$us_r = pg_fetch_assoc($us);
		if (!empty($us_r['id'])) {
			$_SESSION['id'] = $us_r['id'];
		} else {
			$returl = "login?stage=2";
		}
	}
} else if (isset($_POST['old'])) {
	$_SESSION['id'] = uudbLogin($conn,$_POST['login'],$_POST['passwd']);
    if (!empty($_SESSION['id'])) {
        $query = "
            UPDATE
                users
            SET
                email = $2
            WHERE
                id = $1
            ";
        pg_query_params($query,array($_SESSION['id'],$_SESSION['email']));
    }
} else if (isset($_POST['create']) && !empty($_SESSION['email'])) {
    $query = "
        INSERT INTO
            users (
                uuid,
                email,
                lvl,
                karma,
                nick
            ) VALUES (
                uuid_generate_v4(),
                $1,
                $2,
                $3,
                $4
            )
        ";
    pg_query_params($query,array($_SESSION['email'],2,0,$_POST['nick']));
}


if (!$user->loggedIn() && empty($returl))
	$returl = "login?failed=1";

if ($tmp && $_SESSION['uuid'] != $tmp)
	$_SESSION['mon']=0;

?>