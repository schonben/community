<?php

if (!defined("ENTRY") || ENTRY != "post")
    exit("Invalid entry point");

if (empty($_POST['id'])) $_POST['id'] = NULL;
if (empty($_POST['parent'])) $_POST['parent'] = NULL;

$thread = new thread($_POST['thread']);

switch ($_POST['action']) {
    case "post":
        $thread->private = !empty($_POST['private']);
        $thread->announce = !empty($_POST['announce']);
        if (empty($_POST['tags'])) {
            if ($_POST['private'] == "false")    
                $_POST['tags'] = "general";
            else
                $_POST['tags'] = "private";
        }
        $thread->tags = $_POST['tags'];

        if (empty($_POST['recipients']))
            $thread->recipients = array();
        else
            $thread->recipients = explode(",",$_POST['recipients']);

        $thread->subject = $_POST['subject'];

        $thread->save();

    // Flowthru motha
    case "reply":
        $sql = "
            INSERT INTO
                #schema#.posts (
                    thread_id,
                    parent_id,
                    user_id,
                    body
                ) VALUES (
                    :thread,
                    :parent,
                    :user,
                    :body
                )";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(
            ":thread"=>$thread->id,
            ":parent"=>$_POST['id'],
            ":user"=>$user->id,
            ":body"=>$_POST['post'],
            ));
    break;
    case "edit":
        // Fetch old message body to insert into edit history
        $sql = "
            SELECT
                body
            FROM
                #schema#.posts
            WHERE
                uuid = :uuid
                ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(
            ":uuid"=>$_POST['id'],
            ));
        $row = $query->fetch();
        

        // See if there is any change
        if (trim($_POST['post']) != trim($row['body'])) {
            $sql = "
                UPDATE
                    #schema#.posts
                SET
                    body = :body,
                    idx = nextval('community.idx_seq')
                WHERE
                    uuid = :uuid
                    ";
            $query = $GLOBALS['db']->prepare($sql);
            $query->execute(array(
                ":uuid"=>$_POST['id'],
                ":body"=>$_POST['post'],
                ));
    
            $sql = "
                INSERT INTO
                    #schema#.marker_edit (
                        user_id,
                        item_id,
                        body
                    ) VALUES (
                        :user,
                        :item,
                        :body
                    )";
            $query = $GLOBALS['db']->prepare($sql);
            $query->execute(array(
                ":user"=>$user->id,
                ":item"=>$_POST['id'],
                ":body"=>$row['body'],
                ));
        }
    break;
}

$returl = "thread/{$thread->id}#unread0";

?>
