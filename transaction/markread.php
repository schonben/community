<?php

if (empty($_REQUEST['item'])) {
    $fail = "No item id";
    return;
} else {
	$item = $_REQUEST['item'];
}
if (empty($_REQUEST['uid']))
    $uid = $user->id;
else
	$uid = $_REQUEST['uid'];
if (empty($_REQUEST['tags']))
    $_REQUEST['tags'] = "";


if (empty($_REQUEST['idx'])) {
    $sql = "
        SELECT
            max(idx) as idx
        FROM
            #schema#.posts
        ";
    $result = $db->query($sql);
    $row = $result->fetch();
    $idx = $row['idx'];
} else {
    $idx = $_REQUEST['idx'];
}

// Prepare the queries
$sql = "
    SELECT
        idx,
        true_idx
    FROM
        #schema#.marker_read
    WHERE
        item_id = :item
    AND
        user_id = :user
    ";
$select = $db->prepare($sql);
$sql = "
    DELETE
    FROM
        #schema#.marker_read
    WHERE
        item_id = :item
    AND
        user_id = :user
    ";
$delete = $db->prepare($sql);
$sql = "
    INSERT INTO
        #schema#.marker_read (
            item_id,
            user_id,
            idx,
            true_idx
        ) VALUES (
            :item,
            :user,
            :idx,
            :tidx
        )";
$insert = $db->prepare($sql);


if ($item == "all") {
    $sql = "
        SELECT
            threads_tag.uuid
        FROM
            #schema#.threads_tag
        LEFT JOIN
            #schema#.threads_read
        ON
            threads_tag.uuid = threads_read.item_id
        AND
            threads_read.user_id = :user
        LEFT JOIN
            #schema#.threads_link
        ON
            threads_tag.uuid = threads_link.thread
        AND
            threads_link.user = :user
        WHERE
            COALESCE((threads_tag.posts - threads_read.rd),threads_tag.posts,0) > 0
        AND
            (threads_tag.private = false
        OR
            threads_link.thread is not null)
        ".threadlist::tags($_REQUEST['tags']);
    $result = $db->prepare($sql);
    $result->execute(array(":user"=>$uid));
    while ($row = $result->fetch()) {
        markread($row['uuid'],$uid,$idx,false);
    }
    $returl = "communication/{$_REQUEST['tags']}";
} else {
    markread($item,$uid,$idx);    
}

if (empty($returl))
    $noredir = 1;

function markread ($item,$uid,$idx,$trueread=true) {
    global $select, $delete, $insert;
    
    if ($trueread) $tidx = $idx; else {
        $select->execute(array(":item"=>$item,":user"=>$uid));
        $row = $select->fetch();
        if (empty($row['true_idx']))
            $tidx=0;
        else
            $tidx=$row['true_idx'];
    }
    $delete->execute(array(":item"=>$item,":user"=>$uid));
    $insert->execute(array(":item"=>$item,":user"=>$uid,":idx"=>$idx,":tidx"=>$tidx));
}

?>
