<?php

if (!defined("ENTRY") || ENTRY != "post")
    exit("Invalid entry point");

$noredir = true;

switch($_POST['action']) {
    case "sort":
        $sql = "
            UPDATE
                #schema#.dashlets
            SET
                so = :so,
                pos = :pos,
                subpos = :subpos
            WHERE
                uuid = :uuid
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $order = json_decode($_POST['data']);
        foreach ($order as $a => $o) {
            $pos = "";
            $subpos = "";
            switch ($a) {
                case 0:
                    $pos = "sidebar";
                break;
                case 1:
                    $pos = "dashboard";
                    $subpos = "left";
                break;
                case 2:
                    $pos = "dashboard";
                    $subpos = "right";
                break;
            }
            $so = 0;
            foreach ($o as $id) {
                $so += 2;
                $query->execute(array(
                    ":uuid" => $id,
                    ":so" => $so,
                    ":pos" => $pos,
                    ":subpos" => $subpos,
                    ));
            }            
        }
    break;
    case "remove":
        $sql = "
            DELETE
            FROM
                #schema#.dashlets
            WHERE
                uuid = :uuid
            ";
        $query = $GLOBALS['db']->prepare($sql);
        if ($query->execute(array(":uuid"=>$_POST['id'])))
            echo "ok";
    break;
    case "add":
        $uuid = gen_uuid();
        $sql = "
            INSERT INTO
                #schema#.dashlets (
                    uuid,
                    so,
                    pos,
                    user_id,
                    dashlet
                ) VALUES (
                    :uuid,
                    0,
                    'dashboard',
                    :user,
                    :dashlet
                )
            ";
        $query = $GLOBALS['db']->prepare($sql);
        // TODO return uuid for record, and dashlet name
        if ($query->execute(array(":uuid"=>$uuid,":user"=>$user->id,":dashlet"=>$_POST['dashlet']))) {
            echo json_encode(array("status"=>"ok","uuid"=>$uuid,"dashlet"=>$_POST['dashlet']));
        }
    break;
}

?>