<section class="login box">
    <h2 class="title">{"Uplink Community Login"|loc}</h2>
    <div>
        {"Login_text"|loc|local_format}
    </div>
    <hr/>
    <div class="entry">
        <form action="/validate" method="post">
            <input type="hidden" name="fname" value="{$fname}"/>
            <input type="hidden" name="lname" value="{$lname}"/>
            <input type="hidden" name="email" value="{$email}"/>
            <input type="hidden" name="csum" value="{$csum}"/>
            <label for="name">{"Name"|loc}:</label>
            <input type="text" name="name" value="{$fname} {$lname}" disabled="disabled"/><br/>
            <label for="email">{"E-mail"|loc}:</label>
            <input type="text" name="email" value="{$email}" disabled="disabled"/><br/>

            <label for="password">{"Password"|loc}:</label>
            <input type="password" name="pass1" id="password"/><br/>
            <label for="password2">{"Confim Password"|loc}:</label>
            <input type="password" name="pass2" id="password2"/><br/>

            <label for="code">{"Validation Code"|loc}:</label>
            <input type="number" name="code" id="code"/><br/>
            
            <input type="submit" class="button" value="Register"/><br/>
        </form>
    </div>
</section>
