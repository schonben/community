<section class="page box">
	<h2 class="title">{"Welcome to Uplink Community"|loc}</h2>
	<div>
		{"create_text"|loc}
	</div>
	<div class="entry">
		<form action="login.run" method="post">
			<input type="text" name="nick" value="{$nick}"/><br/>
			<input type="submit" name="create" value="Create"/><br/>
		</form>
	</div>
	<div>
		{"old_login_text"|loc}
	</div>
	<div class="entry">
		<form action="login.run" method="post">
			<input type="text" name="login"/><br/>
			<input type="password" name="passwd"/><br/>
			<input type="submit" name="old" value="Login"/><br/>
		</form>
	</div>
</section>
