<script type="text/javascript">
    var tolist = jQuery.parseJSON('{$tolist_json}');
</script>
<div id="tagcloud">
    <h3>{"Tagcloud"|loc}</h3>
    {foreach $tags as $tag}
        <span style="font-size: {$tag.nr|tagsize}pt;" onclick="addTag('{$tag.val}')">{$tag.val|local_format|ucwords}</span>
    {/foreach}
    <br style="clear: left;"/>
    <hr/>
    <input type="hidden" id="tags" name="tags" value="{implode(",",$thread->tags)}"/>
    <input type="text" name="addtag" id="addtag" placeholder="{"Tag"|loc}" class="smalltext"/>
    <input type="button" value="{"Add Tag"|loc}" onclick="addTag(this.form.addtag.value);this.form.addtag.value='';"/><br/>
</div>
<div class="post_wrap">
    <input type="text" name="subject" id="subject" placeholder="{"Subject"|loc}" class="bigtext" value="{$thread->subject}"/><br/>
</div>              
<label for="announce">{"Announcement"|loc}:</label>
<input type="checkbox" name="announce" id="announce" value="true"{if $thread->announce} checked="checked"{/if}/><br/>
<label for="private">{"Private"|loc}:</label>
<input type="checkbox" name="private" id="private" value="true"{if $thread->private} checked="checked"{/if}/><br/>
<div class="recipients post_wrap">
    <span id="recipientsDisplay"></span>
    <input type="text"  onchange="addRecipient(this.value);" name="addrecipient" id="addrecipient" placeholder="{"Recipient"|loc}" list="tolist"/>
</div>
<input type="hidden" id="recipients" name="recipients" value="{implode(",",$thread->getLinks(false))}"/>
<datalist id="tolist">
    {foreach $tolist as $to}
        <option value="{$to.short}{if $to.long != ""} - {$to.long}{/if}"/>
    {/foreach}
</datalist>
<div id="taglist"></div>