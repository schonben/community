<section class="login box">
    <h2 class="title">{"Uplink Community Login"|loc}</h2>
    <div>
        {"Login_text"|loc|local_format}
    </div>
    <hr/>
    <div class="entry">
        <form action="/validate" method="post">
            <label for="fname">{"Firstname"|loc}:</label>
            <input type="text" name="fname" id="fname"/><br/>
            <label for="lname">{"Lastname"|loc}:</label>
            <input type="text" name="lname" id="lname"/><br/>
            <label for="email">{"E-mail"|loc}:</label>
            <input type="text" name="email" id="email"/><br/>

            <input type="submit" class="button" value="Validate"/><br/>
        </form>
    </div>
</section>
