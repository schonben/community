<article class="post_item{if $post->unread} unread{/if}">
    <p class="meta">
        <div class="avatar">
            <img src="http://avatar.ulfls.com/{$post->poster_id}.{$post->sex}.png?h=65&amp;w=80" alt=""/>
        </div>
        <span class="tools">
            {foreach $post->tools as $tool=>$data}
                <a href="{$data.link}"><img src="{$data.icon}" alt="{$tool}" title="{$tool}"/></a>
            {/foreach}
        </span>
        <span>
            Posted by <a href="/user/{$post->poster_id}" class="postedby">{$post->poster}</a>
        </span>
        <time datetime="{$post->dati->format("Y-m-d H:i:s")}">
            <span class="date">{$post->dati->format("Y-m-d")}</span>
            <span class="time">{$post->dati->format("H:i")}</span>
        </time>
    </p>
    <div class="entry" style="padding-left: {$post->level*5}px;">
        <span class="bodytext">
        {$post->body|local_format}
        </span>
        {if ($post->sig != "")}
            <div class="sig">{$post->sig|local_format}</div>
        {/if}
    </div>
</article>