<section class="login box">
	<h2 class="title">{"Uplink Community Login"|loc}</h2>
	<div>
		{"Login_text"|loc|local_format}

        <a href="/register">{"Register"|loc}</a>
    </div>
	<hr/>
    <div class="entry">
        <form action="/" method="post">
            <input type="hidden" name="loginform" value="1"/><br/>
            <label for="username">{"Email or Username"|loc}:</label>
            <input type="text" name="login" id="username"/><br/>
            <label for="password">{"Password"|loc}:</label>
            <input type="password" name="passwd" id="password"/><br/>
            <label for="remember">{"Remember Login"|loc}:</label>
            <input type="checkbox" name="remember" id="remember" value="1"/><br/>
            <input type="submit" class="button" value="{"Login"|loc}"/><br/>
        </form>
    </div>
</section>
