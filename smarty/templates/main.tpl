<section id="wrapper">
	<header>
		<nav id="status">
            <span>
                {"Logged in as"|loc} {$user->displayName()}
            </span>
            <ul>
                <li><a href='settings'>{"Settings"|loc}</a></li>
                <li><a href='?logout=1'>{"Logout"|loc}</a></li>
            </ul>
		</nav>
		<section id="topbar">
			<img id="logo" src="/layout/{$layout}/images/logo.png"/>
			<h1>{$site_title}</h1>
			<p>{$title}</p>
            <div class="search">
                {include file="searchbox.tpl" query=$query}
            </div>
		</section>
		<nav id="navigation">
			{$menu}
		</nav>
	</header>
	<aside id="widget">
	    <ul class="dashlet sidebar">
            {$dashlet}
	    </ul>
	</aside>
	<aside id="toolbar">
		{foreach $toolbar as $tool}
            <a href="{if empty($tool.url)}javascript:void(0){else}{$tool.url}{/if}"{if !empty($tool.onclick)} onclick="{$tool.onclick}"{/if} title="{$tool.txt}">
				<img src="/layout/{$layout}/images/icons/toolbar/{$tool.icon}.png" alt="{$tool.txt}"/>
			</a>
		{/foreach}
	</aside>
	<main>
		{if $template != ""}
		  {include file="$template.tpl"}
		{/if}
	</main>
</section>
<footer>
	<span>
		Hosted by Uplink Data Finland Oy Ab
	</span>
</footer>
