{extends "dashlet_standard.tpl"}

{block name="content"}
    {foreach $data as $row}
    	<div class="dashlet_announce">
    		<span class="tags">
    			{foreach $row.tags as $tag}
    				<span class="dashlet_tagitem">{$tag}</span>
    			{/foreach}
    		</span>
    		<span class="dashlet_icon"><img src="/layout/{$setting.layout}/images/icons/announce.png" alt="" style="width: 32px;"/></span>
    		<span class="dashlet_poster">{$row.poster}</span>
    		
    		<h3 class="dashlet_title {$row.class}">
    		    <a href="{$row.link}">
            		<time datetime="{$row.dati}">
                        <span class="dashlet_date">{$row.dat}</span> - <span class="dashlet_time">{$row.tim}</span>
                    </time>
                    - {$row.subject}
                </a>
            </h3>
    		
    		<span class="dashlet_body">{$row.body|local_format}</span>
    	</div>
    {/foreach}
{/block}
