<span class="controls">
    <img src="/layout/{$layout}/images/icons/remove.png" alt="X" onclick="removeDashlet($(this))" />
</span>
<h2>
    {$title|loc}
</h2>

{block name="content"}
<table class="dashlet standard">
	{foreach $data as $row}
		<tr class="{$row.class}">
		{foreach $row.cols as $c => $col}
			<td>{if ($row.link && in_array($c,$row.links))}<a href="{$row.link}">{/if}{$col}{if ($row.link && in_array($c,$row.links))}</a>{/if}</td>
		{/foreach}
		</tr>
	{/foreach}
</table>
{/block}
