<div class='pageflip'>
	<a href='?tag={$tag}&amp;page={$page-1}'>&lt;&lt; {"Previous"|loc}</a>
	<span>{"Page"|loc} {$page} {"of"|loc} {$total}</span>
	<a href='?tag={$tag}&amp;page={$page+1}'>{"Next"|loc} &gt;&gt;</a>				
</div>
<table class='list'>
	<tr>
		<th colspan="2">{"Subject"|loc}</th>
		<th>{"Tags"|loc}</th>
		<th>{"Unread"|loc}</th>
		<th>{"Posts"|loc}</th>
		<th>{"Last Post"|loc}</th>
	</tr>
	{foreach $rows as $row}
	{if $row.unread}{$ur="unread"}{else}{$ur=""}{/if}
	<tr>
        <td class='{$ur}'>
            {if $row.announce}
                <img src="/layout/satlink4/images/icons/announce_tn.png" alt="" title="{"Announcement"|loc}"/>
            {else}
                <img src="/layout/satlink4/images/icons/message_tn.png" alt="" title="{"Message"|loc}"/>
            {/if}
        </td>
		<td class='{$ur}'><a href='/thread/{$row.id}#unread0'>{$row.subject|local_format}</a></td>
		<td class='{$ur}'>{foreach $row.tags as $t}<a href='/communication/{$t|urlencode}'>{$t|ucwords|local_format}</a> {/foreach}</td>
		<td class='center short {$ur}'>{$row.unread}</td>
		<td class='center short {$ur}'>{$row.posts}</td>
		<td class='center short {$ur}'>{$row.lp}</td>
	</tr>
	{/foreach}
</table>
<div class='pageflip'>
	<a href='?tag={$tag}&amp;page={$page-1}'>&lt;&lt; {"Previous"|loc}</a>
	<span>{"Page"|loc} {$page} {"of"|loc} {$total}</span>
	<a href='?tag={$tag}&amp;page={$page+1}'>{"Next"|loc} &gt;&gt;</a>				
</div>
