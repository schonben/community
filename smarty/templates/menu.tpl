<ul>
{foreach $men as  $key => $val}
	<li>
		<a href='{$val.link}'>{$key|loc}</a>
		<ul>
		{foreach $val.sub as $skey => $sval}
			<li>
				<a href='{$sval}'>{$skey|loc}</a>
			</li>
		{/foreach}
		</ul>
	</li>
{/foreach}
</ul>