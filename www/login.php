<?php

define("ENTRY","login");

$logover = 1;
include("../globals/init.php");
include("../globals/init_display.php");

switch ($template) {
    case "register":
        $pg_title = loc("Register");
        $display = 'register.tpl';
    break;
    case "validate":
        $time = floor(time()/300);
        if (empty($_POST['code'])) {
            if (empty($_SESSION['validationcode'][$_POST['email']]))
                $_SESSION['validationcode'][$_POST['email']] = rand(100000,999999);
            
            mail($_POST['email'],"Uplink E-mail validation","Enter this validation code: {$_SESSION['validationcode'][$_POST['email']]}");
    
            $smarty->assign("fname",$_POST['fname']);
            $smarty->assign("lname",$_POST['lname']);
            $smarty->assign("email",$_POST['email']);
    
    
            $smarty->assign("csum",sha1($time.$_POST['email'].$_SESSION['validationcode'][$_POST['email']]));
        } else {
            if (($_POST['csum'] == sha1($time.$_POST['email'].$_POST['code']) || $_POST['csum'] == sha1(($time-1).$_POST['email'].$_POST['code'])) && $_POST['code'] == $_SESSION['validationcode'][$_POST['email']]) {
                $_SESSION['userid'] = $user->register($_POST['email'],$_POST['fname'],$_POST['lname'],$_POST['pass1'],$_POST['pass2'],true);
                header("Location: http://".$_SERVER["HTTP_HOST"]."/");
                exit();
            }            
            die("Invalid Code");
            // Invalid Code
        }
        
        $pg_title = loc("Validate");
        $display = 'validate.tpl';
    break;
    default:
        $pg_title = loc("Login");
        $display = 'login.tpl';
}

include("../headers/header.php");

$smarty->display($display);

include("../headers/footer.php");

?>