$(function() {
    $("ul.dashlet").sortable({
        connectWith: "ul.dashlet",
        handle: "h2",
        stop: function( event, ui ) {saveDashlets(ui);},
    }).disableSelection();
});

function loadDashlet (dash,uid,area) {
	$.get("/dashlet/"+dash, {area: area},
	function(data){
		$("#"+uid).html(data);
	});
}
function saveDashlets(ui) {
    var order = Array(Array(),Array(),Array());
    $(".sidebar li").each(function(){
        order[0].push($(this).attr("id"));
    });
    $(".left li").each(function(){
        order[1].push($(this).attr("id"));
    });
    $(".right li").each(function(){
        order[2].push($(this).attr("id"));
    });
    $.post("/dashlet.do", { action: "sort", data: JSON.stringify(order) } );
}
function removeDashlet(element) {
    var id = element.parent().parent().attr("id");
    if (confirm("Are you sure?")) {
        $.post("/dashlet.do", { action: "remove", id: id }, function (data) {
            if (data == "ok")
                $("#"+id).remove();
        });
    }
}
function showNewDashlet (element) {
    if ($(".newdashlets").hasClass("hidden")) {
        $(".newdashlets").removeClass("hidden");
    } else {
        $(".newdashlets").addClass("hidden");
    }
}
function addDashlet(dashlet) {
    $(".newdashlets").addClass("hidden");
    $.post("/dashlet.do", { action: "add", dashlet: dashlet }, function (data) {
        var obj = jQuery.parseJSON(data);
        if (obj.status == "ok") {
            $.get("/dashlet/"+obj.dashlet, function (data) {
                var html = '<li id="'+obj.uuid+'" class="box dashlet dashlet_'+obj.dashlet+'">'+data+'</li>';
                $("#col_left").prepend(html);
            })
        }
    });
}
