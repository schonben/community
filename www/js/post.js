var selectedInputArea;
var selectedPos = 0;

function addTag (tag) {
	tg = tag.toLowerCase();
	tags = readValues("tags");
	if ($.inArray(tg,tags) == -1) {
		tags.push(tg);
		tags.sort();
		$("#tags").val(tags.join(','));
        drawCloud();
	}
}
function delTag (tag) {
    tags = readValues("tags");
	tags.splice($.inArray(tag,tags),1)
	$("#tags").val(tags.join(','));
    drawCloud();
}
function drawCloud () {
    tags = readValues("tags");
	$("#taglist").html("");
	$.each(tags, function (index,value) {
		$("#taglist").append(" <span onclick='delTag(\""+value+"\");'>"+value+"</span> ");
	})
}

function addRecipient (val) {
    $("#addrecipient").val("");
    var recipients = readValues("recipients");
    var index = val.split(' - ');
    var uuid = "";
    $.each(tolist,function(id,value) {
        if (value["short"].toLowerCase() == index[0].toLowerCase()) {
            uuid = id;
        }
    });
    if ($.inArray(uuid,recipients) == -1) {
        recipients.push(uuid);
        $("#recipients").val(recipients.join(','));
        drawRecipients();
    }
}
function delRecipient (uuid) {
    var recipients = readValues("recipients");
    recipients.splice($.inArray(uuid,recipients),1)
    $("#recipients").val(recipients.join(','));
    drawRecipients();
}

function drawRecipients () {
    var recipients = readValues("recipients");
    $("#recipientsDisplay").html("");
    $.each(recipients, function (index,value) {
        $("#recipientsDisplay").append(" <span onclick='delRecipient(\""+value+"\");'>"+tolist[value]["short"]+"</span> ");
    })
}

function readValues(id) {
    if ($("#"+id).val().length)
        values = $("#"+id).val().split(',');
    else
        values = Array();
    return values;    
}


function searchArr () {
	$.each(tolist, function(index, value) {
		
	});
}

var blockform = false;
$(document).ready(function() {
	$('#addtag').keypress(function(event) {
		if (event.keyCode == 13) {
			blockform = true;
			addTag($('#addtag').val());
			$('#addtag').val('');
		}
	});
    $('#addrecipient').keypress(function(event) {
        if (event.keyCode == 13) {
            blockform = true;
            addRecipient($('#addrecipient').val());
            $('#addrecipient').val('');
        }
    });
	$("#postform").submit(function () {
		if (blockform) {
			blockform = false;
			return false;
		}
	});
    drawCloud();
    drawRecipients();
});

function doInsert(text) {
    var oldtext = selectedInputArea.value;
    pretext = oldtext.substring(0,selectedPos);
    posttest = oldtext.substring(selectedPos,oldtext.length);
    selectedInputArea.value = pretext + text + posttest;
}

function quote (f) {
	selectedInputArea = f.post;
	doInsert('[quote='+$(".postedby").html().trim()+']'+$("#bodytext").html().trim()+'[/quote]\n');
}

