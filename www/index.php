<?php

define("ENTRY","index");

$starttime = microtime(true);

include("../globals/init.php");
include("../globals/init_display.php");

ob_start();
try {
    if (file_exists("../templates/{$template}.php"))
        include("../templates/{$template}.php");
    $scriptoutput = ob_get_clean();
    ob_end_clean();
} catch (Exception $e) {
    // TODO make a better error handler
    ob_end_clean();
    echo "Uncaught exception:\n".$e->getMessage();
    exit();
}

if (!empty($scriptoutput)) {
    echo "Unexpected script output: ";
    echo $scriptoutput;
    exit();
    error_log($scriptoutput);
}

include("../globals/layout_proc.php");

$output = $smarty->fetch("main.tpl");

include("../headers/header.php");

echo "\n\n<!-- Template returned in ".round(microtime(true)-$starttime,3)." seconds -->\n\n";

echo $output;

include("../headers/footer.php");

?>