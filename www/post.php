<?php

define("ENTRY","post");

include("../globals/init.php");

$returl = "";
$fail = "";

$db->beginTransaction();

try {
    include("../transaction/{$template}.php");
} catch (Exception $e) {
    // TODO make a better error handler
    echo "Uncaught exception:\n".$e->getMessage();
    exit();
}


if ($db->isFail()) {
    $db->rollBack();
//	die($db->fail);
}
$db->commit();

if (empty($_GET['noredir']) && empty($noredir))
    header("Location: http://".$_SERVER["HTTP_HOST"]."/".$returl);

?>
