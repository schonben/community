<?php

define("ENTRY","dashlet");

$noaction = 1;
require("../globals/init.php");
require("../globals/init_display.php");
if (empty($_REQUEST['area'])) $area = false; else $area = $_REQUEST['area'];

$class = $id."Dashlet";
if (classloader($class,"dashlets") && $user->isLoggedIn()) {
    $dashlet = new $class($area);
    $script = $dashlet->script;
	echo $dashlet->display();
} else {
	header("HTTP/1.0 404 Not Found");
echo '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>Dashlet not found</p>
</body></html>';
}

?>