<?php

if (strstr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml")) {
	$ct = "application/xhtml+xml";
} else {
	$ct = "text/html";
}
echo header("Content-type: {$ct}; charset=utf-8"); 
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

$css = array(
    "style",
    "dashlet",
    "nav",
    $template,
    );

$css_html = "";
foreach (array_merge($css,$extra_css) as $ecss) {
    $filename = "/layout/{$setting['layout']}/{$ecss}.css";
    if (file_exists($setting['apath']."/www".$filename))
        $css_html .= "
            <link href='$filename' title='' rel='stylesheet' type='text/css' media='screen'/>";
    foreach (array("1024","800","640") as $size) {
        $filealt = "/layout/{$setting['layout']}/{$ecss}_{$size}.css";
        if (file_exists($setting['apath']."/www".$filealt))
            $css_html .= "
            <link href='{$filealt}' title='' rel='stylesheet' type='text/css' media='screen and (max-width:{$size}px)'/>";
    }

}

$ejs = "";
foreach ($extra_js as $js) {
	$ejs .= "
		<script type='text/javascript' src='/$js'></script>
	";	
}

echo <<<HTML
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<meta http-equiv="content-type" content="{$ct}; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>{$setting['title']} - {$pg_title}</title>
		<link rel="shortcut icon" href="/images/fav.png" type="image/png" />
        <link rel="stylesheet" href="/css/reset.css" />
		{$css_html}

        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
		<script src="/js/html5.js" type="text/javascript"></script>
		<script src="/js/dashlet.js" type="text/javascript"></script>
		{$ejs}
	</head>

	<body>
HTML;
	
?>