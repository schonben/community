<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

// Dashlets
$area = "sidebar";			

$script = "";
$dash = "";
foreach (dashlet::getDashlets($user,$area) as $dashlet) {
    $script .= $dashlet->script;
    $dash .= <<<DASHLET
            <li id="{$dashlet->uniq}" class="box dashlet dashlet_{$dashlet->name}">
                {$dashlet->display()}
            </li>
DASHLET;
    $script .= $dashlet->getTimer();
    
}
$dash .= <<<DASHLET
<script type="text/javascript">
$script
</script>
DASHLET;
// End Dashlets

if (empty($pg_toolbar))
	$pg_toolbar = Array();

include("../globals/menu.php");
$smarty->assign('men', $men);

if (empty($_GET['query']))
	$_GET['query'] = "";

$smarty->assign('toolbar', $pg_toolbar);
$smarty->assign('menu', $smarty->fetch('menu.tpl'));
$smarty->assign('site_title', $setting['title']);
$smarty->assign('title', $pg_title);
$smarty->assign('query', $_GET['query']);
$smarty->assign('dashlet', $dash);
//$smarty->assign('page', $output);

//$page = $smarty->fetch('main.tpl');

?>
