<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

$men = array();


$men["dashboard"] = array("link" => "/","sub" => array());
$men["communication"] = array("link" => "/communication","sub" => array("tags" => "/tags", "new post" => "/post",));
if ($setting['dev']) $men["gallery"] = array("link" => "/gallery","sub" => array());
$men["groups"] = array("link" => "/groups","sub" => array());

if ($user->lvl >= 10)
	$men["admin"] = Array("link" => "admin","sub" => Array("tag management" => "tagmanagement","locale" => "locale"));

?>