<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");


function local_format($text,$youtube=1) {
	global $setting;

	$text = xml_format($text,1);

	$text = bbcode($text);

	// Quotes
	$text = quote($text);

	// Autolinker
	$text = preg_replace (";([^>'\"]|^)(https?://[^ ,<\n\r]+);i","\\1<a href='\\2' target='_blank' class='autolink'>\\2</a>", $text);
	// E-mail autolinker
	$text = preg_replace (";([a-z0-9]+[a-z0-9\._-]*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+);i","<a href='mailto:\\1' class='autolink'>\\1</a>", $text);

	// Youtube auto embedder
	if ($youtube)
		$text = preg_replace(";<a[^>]+>https?://[a-z]+\.youtube\.[a-z]+/watch\?v=([^&<]+)[^<]*</a>;",'<iframe title="YouTube video player" width="640" height="510" src="http://www.youtube.com/embed/\1" frameborder="0" allowfullscreen="allowfullscreen"></iframe>',$text);
	// Youtube BBcode
	$text = preg_replace("|\[youtube\]([^\[]*)\[/youtube\]|",'<iframe title="YouTube video player" width="640" height="510" src="http://www.youtube.com/embed/\1" frameborder="0" allowfullscreen="allowfullscreen"></iframe>',$text);
	
	return $text;
}

function xml_format ($tex,$br=0) {
	$tex = safe_xml_entities($tex,ENT_QUOTES ,"UTF-8");
	// $tex = html2xml($tex);
	if ($br) {
		$tex = str_replace(chr(10),"<br/>".chr(10),$tex);
	}
	$tex = str_replace("&#91;&#91;","<",$tex);
	$tex = str_replace("&#93;&#93;",">",$tex);
	$tex = preg_replace("/<&#47;(.*)>/","</\\1>",$tex);
	return $tex;
}

function ulError ($errno, $errstr, $errfile, $errline) {
    if (!empty($GLOBALS['errorhandlerrun'])) {
        exit("Error handler crashed");
    }
    $GLOBALS['errorhandlerrun'] = 1;
        
    $error = debug_backtrace(); 

    $output = "
        <h1>({$errno}) {$errstr}</h1>
        <p>
            An error occured in <b>{$errfile}</b> on line: <b>{$errline}</b>
        </p>
        <div style='border: 1px solid black;'>

        ";
    foreach ($error as $key => $val) {
        if (empty($val['file'])) $val['file'] = "";
        if (empty($val['line'])) $val['line'] = 0;
        
        $output .= "<div style='border: 1px solid black; margin: 10px;'><h2>{$key}. {$val['file']} line {$val['line']} </h2>";
            if (isset($val['args'])) foreach ($val['args'] as $i => $err) {
                $output .= "<div>";
                if (is_array($err)) {
                    if (isset($err['setting']['db']['password'])) $err['setting']['db']['password'] = "xxxxxxxxx";
                    $out = nl2br(htmlspecialchars(print_r($err,true)),true);
                    $output .= "<div style='font-size: 0.6em; background-color: #edebaf'>{$out}</div>";
                } else if (is_object($err)) {
                    $output .= print_r($err,true);
                } else {
                    $output .= "{$err}";
                }
                $output .= "</div>";   
            }
        $output .= "</div>";
    }
    $output .= "</div>";

    if (empty($GLOBALS['setting']['dev'])) {
        echo "An error occured!";        
    } else {
        echo $output;
    }
    
    exit();
}

function classloader ($class,$path="classes") {
    if (class_exists($class)) {
        return true;
    } else {
        if (@include_once "../{$path}/{$class}.class.php")
            return true;
        else
            return false;    
    }
}

function tagsize($nr,$base=8) {
    return round($base+log($nr));    
}

?>