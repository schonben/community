<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

session_start();
mb_language("uni");

if (empty($_SESSION['userid']))
	$_SESSION['userid'] = "";
if (empty($_SESSION['monitor']))
	$_SESSION['monitor'] = 0;

include "functions.inc";
include "uuid.inc";
include "locale_functions.inc";
include "local_functions.php";
include "settings.php";

spl_autoload_register('classloader',true,true);
//set_error_handler("ulError");

include "foondb/db.class.inc";
include "uudb.class.inc";
include "uudbuser.class.inc";

$dsn = "{$setting['db']['type']}:dbname={$setting['db']['name']};host={$setting['db']['server']}";
$GLOBALS['db'] = new db($dsn,$setting['db']['user'],$setting['db']['password']);
$GLOBALS['db']->schema($setting['schema']);


include($setting['localepath']."/locale.inc");
include($setting['localepath']."/locale_list.inc");

if (!empty($_REQUEST['template'])) {
    $template = $_REQUEST['template'];
} else {
    $template = "index";
}

$monitor = new uudb($_SESSION['monitor']); 

$user = new user($_SESSION['userid'],true,(ENTRY == "index"));
if (isset($_POST['login']) && isset($_POST['passwd']) && isset($_POST['loginform'])) {
    $_SESSION['userid'] = $user->login($_POST['login'],$_POST['passwd'],!empty($_POST['remember']));
}

// Logout
if (!empty($_REQUEST['logout'])) {
    $user->logout($_SESSION['userid']);
    header("Location: http://".$_SERVER["HTTP_HOST"]."/login");
    exit();
}

// TODO update heartbeat (should be done in user class)

//$_SESSION['mon'] = log_session($conn,$site,$_SESSION['id'],$_SESSION['mon']);

if (!$user->isLoggedIn() && ENTRY != "login") {
	header("Location: http://".$_SERVER["HTTP_HOST"]."/login");
	exit();
}

// Locale Stuff
if (isset($_GET['cloc'])) {
	$_SESSION['locale'] = $_GET['cloc'];
	setcookie(loc,$_GET['cloc'],time()+60*60*24*300);
}

//if ($lvl >= 4)
//	$lca = $setting[locs];
//else
	$lca = $setting['show_locs'];

if (empty($_SESSION['locale'])) {
	if (isset($_COOKIE['loc'])) {
		if (in_array($_COOKIE['loc'],$lca))
			$_SESSION['locale'] = $_COOKIE['loc'];
	} else {
		$cl_loc=explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
		$def_loc=substr($cl_loc[0],0,2);
		if (strlen($def_loc) && in_array($def_loc,$lca))
			$_SESSION['locale'] = $def_loc;
		else
			$_SESSION['locale'] = "en";
	}
}
$locale = $_SESSION['locale'];
$locn = array_search($locale, $setting['locs']);
$loc = poploc($locale);
// End Locale Stuff

if (empty($_REQUEST['id'])) {
    $id = "";
} else {
    $id = $_REQUEST['id'];
}

?>
