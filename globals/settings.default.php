<?php

// Database Settings
$setting['db']['type'] = ""; // mysql or pgsql
$setting['db']['server'] = "";
$setting['db']['port'] = 5432; // 3306 for mysql & 5432 for postgresql
$setting['db']['user'] = "";
$setting['db']['password'] = "";
$setting['db']['name'] = "";

$setting['apath']="";
$setting['title']="Uplink Community Dev";
$setting['schema']="community";
$setting['layout'] = "satlink4";
$setting['avatarpath']="";
$setting['localepath']="";
$setting['recaptcha']['publickey']="";
$setting['recaptcha']['privatekey']="";

$setting['dev']=1;
$site=1;

$setting['locs'] = Array("en","sv","fi");
$setting['show_locs'] = Array("en","sv","fi");

$setting['dashlets']['dashboard'] = array("posts","bday");
$setting['dashlets']['sidebar'] = array("user","unread");

?>
