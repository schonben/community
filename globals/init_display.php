<?php

if (!defined("ENTRY"))
    exit("Invalid entry point");

include("basic_html.inc");
include("xml_extra_functions.inc");

require('Smarty-3.1.11/libs/Smarty.class.php');
$smartyBase = new Smarty();

$smartyBase->setTemplateDir(Array($setting['apath'].'/www/layout/'.$setting['layout'],$setting['apath'].'/smarty/templates',$setting['apath'].'/templates'));
$smartyBase->setCompileDir($setting['apath'].'/smarty/templates_c');
$smartyBase->setCacheDir($setting['apath'].'/smarty/cache');
$smartyBase->setConfigDir($setting['apath'].'/smarty/configs');

$smartyBase->muteExpectedErrors();

$smartyBase->assign('setting',$setting);
$smartyBase->assign('user',$user);
$smartyBase->assign('layout', $setting['layout']);

$smarty = clone $smartyBase;

if ($smarty->templateExists($template.".tpl")) {
    $smarty->assign("template",$template);
} else {
    $smarty->assign("template","");
}

$extra_js = array();
$extra_css = array();
$pg_title = "";
if (!empty($template))
    $pg_title = loc($template);

?>